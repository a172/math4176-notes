# Part 14

**STARTING ON PAGE 4**

### Try $w = 537$

Algorithm fails.
We only learned that $(-1)^2 \equiv 1 \pmod n$, which we already knew.

### Try $w = 109$

$1^2 \equiv 813^2 \pmod n$, so $\gcd(813 \pm 1, n)$ gives a non-trivial factor.

## Choosing Encryption Exponent

There are two standard choices of encryption exponent in RSA:
- $2^8 + 1 = 257$
- $2^16 + 1 = 65537$

Two reasons:
1. Each number is a prime, and therefore is likely to be invertible mod
   $\phi(n)$ (unless one divides $\phi(n)$).
2. Write them in binary:
    - $2^8 + 1 = \texttt{0b100000001}$
    - $2^{16} + 1 = \texttt{0b10000000000000001}$

    Many 0's in binary representation means fewer multiplications needed when
    encrypting.
