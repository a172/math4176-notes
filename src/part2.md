# Part 2: More Number Theory

## Euclidean Algorithm

The Euclidean Algorithm is a simple, efficient algorithm for calculating the gcd
of two integers.
Suppose we want to find the gcd of $a$ and $b$, $a > b$.
The basic idea is to divide the smaller integer into the larger to produce a
remainder, and repeat the procedure using the divisor and remainder in place of
$a$ and $b$.

> **Euclidean Algorithm**
>
> *Think of the this algorithm as generating a sequence of remainders.
> As long as the current remainder is nonzero, divide the current remainder into
> the previous to produce a smaller remainder.*
>
> Input: integers $a > b > 0$
> - Set $r_0 = a$, $r_1 = b$, $m = 1$
> - While $r_m \ne 0$ do:
>   - $q_m = \left\lfloor \frac{r_m-1}{r_m} \right\rfloor$
>   - $r_{m+1} = r_{m-1} - q_m r_m$
>   - $m = m + 1$
> - Return $r_{m-1}$
>
> It is easier to follow in a [video](https://youtu.be/JUzYl1TYMcU).

Note that the second equation in the loop can also be written as:
$$ r_{m-1} = q_m r_m + r_{m+1} $$

Since $r_m$ goes into $r_{m-1}$ the integer $q_m$ times, $0 \le r_{m+1} < r_m$.

**Example**

Find $\gcd(1134, 525)$:
$$\begin{aligned}
  1134 &= 525 \cdot 2 + 84 \\
  525 &= 84 \cdot 6 + 21 \\
  84 &= 21 \cdot 4 + 0 \\
\end{aligned}$$
$$
  \gcd(1134, 525) = 21
$$

We can check the correctness easily int this case, since the numbers are small
enough to factor into primes.
Note $1134 = 2 \cdot 3^4 \cdot 7$ and $525 = 3 \cdot 5^2 \cdot 7$.

We can back-solve to find 21 as a linear combination of the two original inputs.

Note:
$$ 21 = 525 - 6 \cdot 84 $$
But:
$$ 84 = 1134 - 2 \cdot 525 $$
So:
$$\begin{aligned}
  21 &= 525 - 6 (1134-2 \cdot 525) \\
  21 &= 13 \cdot 525 - 6 \cdot 1134
\end{aligned}$$

More generically, we can always obtain $\gcd(a, b)$ as a linear combo of $a$ and
$b$ by back-solving through the equations obtained in the Euclidean Algorithm.
There's a more efficient way, the Extended Euclidean Algorithm.

Note that in the case that $\gcd(a, b) = 1$ we obtain an equation of the form
$ax + by = 1$ where $x, y \in \Z$.
This implies $by \equiv 1 \pmod a$, i.e., $b$ is invertible mod $a$.
Moreover, $y \equiv b^{-1} \pmod a$.

**Example**

Is 10 invertible in $\Z_{209}$?
If so, find $10^{-1}$.

$$\begin{aligned}
  209 &= 10 \cdot 20 + 9 \\
  10 &= 9 \cdot 1 + 1 &\exists 10^{-1} \because \gcd(209, 10) = 1 \\
  1 &= 10 + 9(-1) &\textnormal{Solve for the remainder} \\
  1 &= 10 + (209 + 10 \cdot -20)(-1) &\textnormal{Solve and substitute 9} \\
  1 &= 10 + 209(-1) + 10(20) \\
  1 &= 10(21) + 209(-1)
\end{aligned}$$

Since $209k \equiv 0 \pmod{209}$ for any $k \in \Z$,
$10 \cdot 21 \equiv 1 \pmod {209}$.
That is, in $\Z_{209}$:

$$10^{-1} = 21$$

Verifying this:
$$ 21 \cdot 10 = 210 \equiv 1 \pmod{209} $$

### Off-script: Alternate implementation of EEA
For finding $x$ and $y$ such that $\gcd(a, b) = xa + yb$.

Initial conditions:
$$\begin{aligned}
  x_0 &= 1 & y_0 &= 0 \\
  x_1 &= 0 & y_1 &= 1 \\
  q_1 &= \left\lfloor \frac ab \right\rfloor
\end{aligned}$$

Where $ax_i + by_i = r_i$.

| iteration | 0 | 1 |  2 |  3 |
|-----------|---|---|----|----|
| $x_i$     | 1 | 0 |  1 | -6 |
| $y_i$     | 0 | 1 | -2 | 13 |
| $q_i$     |   | 2 |  6 |  4 |

Update rule:
$$\begin{aligned}
  q_i &= \left\lfloor \frac ab \right\rfloor \\
  x_i &= -q_{i-1} \cdot x_{i-1} + x_{i-2} \\
  y_i &= -q_{i-1} \cdot y_{i-1} + y_{i-2} \\
\end{aligned}$$

$
\begin{bmatrix}
x_i \\
y_i
\end{bmatrix}
=
\begin{bmatrix}
x_{i-2} \\
y_{i-2}
\end{bmatrix}
-
q_{i-1}
\begin{bmatrix}
x_{i-1} \\
y_{i-1}
\end{bmatrix}
$

## Euclid's Lemma Proof

Given:
- $p \mid ab$
- $\gcd(a, p) = 1$

WTS: $p \mid b$

Let $ax + py = 1$.
Then:
$$\begin{aligned}
  ax + py &= 1 \\
  \exists x, y &\in \Z &&\because \gcd(a, p) = 1 \\
  (ax + py)b &= (1)b \\
  axb + pyb &= b \\
  p &\mid axb &&\because p \mid ab \land x \in \Z \\
  p &\mid pyb &&\because p \mid p \land b,y \in \Z \\
  p &\mid b &&\because p \mid axb \land p \mid pyb
\end{aligned}$$

## Chinese Remainder Theorem

Suppose that $m_1, \ldots, m_2$ are pairwise coprime.
Then the system of congruences $x \equiv a_i \pmod{m_i}$ has unique solution
modulo $M = m_1 \cdot \ldots \cdot m_r$ given by:
$$
x = \sum_{i=1}^r a_i M_i y_i
$$
where:
- $M_i = \frac{M}{m_i}$
- $y_i = M_i^{-1} \mod m_i$
### Example

Solve the system:
$$\begin{aligned}
  x &\equiv 3  &\pmod 7 \\
  x &\equiv 1  &\pmod{11} \\
  x &\equiv 10 &\pmod{13}
\end{aligned}$$


$$\begin{aligned}
  x &= 3 + 7a \\
  3 + 7a &= 1 &\pmod{11} \\
  7a &= -2    &\pmod{11} \\
  7a &= 9     &\pmod{11} \\
  8 \cdot 7a &= 8 \cdot 9 &\pmod{11} \\
  a &= 6 &\pmod{11}
\end{aligned}$$

Thus, for the first 2 equations:
$$\begin{aligned}
  x &= 3 + 7 \cdot 6 \\
  x &= 45
\end{aligned}$$

But, we can add $7k_1$ and still satisfy the first equation.
Similarly, we can add $11k_2$ and still satisfy the second equation.

So, $x = 45 + 77b$. Then repeat the process to find a value that also satisfies
the final equation.

### Proof

First, show $x$ is a solution for $x \equiv M_i \pmod{m_i}$ for all
$1 \le i \le r$.
For example, when $i = 1$, not $M_j$ is a multiple of $M_1$ for all
$2 \le j \le r$.

Ex:
$M_2 = M_1M_3 \ldots M_n$.
Now note $a_1 \cdot M_1 \cdot M_1^{-1} \equiv a_1 \pmod{m_1}$.
Combining $x \bmod m_1 = a_1 + 0 + \ldots + 0 = a_1$.

Next, show why the solution is unique mod $M$.
Suppose that $x$ and $y$ are both solutions of the system.
Then:

$$
x \equiv y \pmod{m_1} \\
x \equiv y \pmod{m_2} \\
\vdots \\
x \equiv y \pmod{m_r} \\
$$

This implies:
$$
M_1 \mid (y - x) \\
M_2 \mid (y - x) \\
\vdots \\
M_r \mid (y - x) \\
$$

Then $\lcm(m_1, \ldots, m_r) \mid (y - x) \implies M \mid (y - x)$.


### No solutions

What if $\gcd(m_1, m_2) \ne 1$?
Solution is not guaranteed.
Example:
$$
x \equiv 3 \mod 15 \\
x \equiv 4 \mod 20 \\
$$
