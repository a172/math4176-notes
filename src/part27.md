# Part 27: Secret-Sharing Schemes

Alice an Bob have received another inheritance.
Uncle Moneybags didn't trust either of them with the combination to the safe
deposit box, so he decided to split it between them, so they can pool their
knowledge and open the safe.
Is this possible without revealing any info to either of them?

## Secret Splitting
- Encode combination as an integer.
- A solution: give Alice a random integer $r$ and Bob $M - r$
- To reconstruct $M$, they add their pieces together.
- Technical difficulty: randomly choosing the integer
- Choose $n$ larger than all possible messages $M$ and work in $\Z_n$.
- Alice gets random $r \in \Z_n$, Bob gets $(M - r) \bmod n$.
- Add shares and reduce mod $n$ to recover $M$.
- To split among Alice, Bob, and Charles, assign random $r$ to Alice, $s$ to
  Bob, $M - r - s \bmod n$ to Charles.
- Can split a secret $M$ among $j$ people.
  Give $r_i$ to first $j - 1$, $\paren{M - \sum_{i=1}^{j-1} r_i} \bmod n$ to
  $j$th.

## Threshold Schemes

**Definition:**
Let $t, w$ be positive integers with $1 < t \le w$.
A $(t, w)$-threshold scheme is a method of sharing a message among a set of $w$
participants such that any subset consisting of $t$ participants can reconstruct
the message $M$, but no subset of smaller size can reconstruct $M$.

The first method we see is known as the Shamir threshold scheme.
Choose a prime $p$ which must be larger than all possible messages and larger
than the number $w$ of participants.
All computations will be carried out mod $p$.
The message $a_0$ is represented as a number mod $p$, and we want to distribute
it among $w$ people in such a way that $t$ of them are needed to reconstruct the
message.
First, randomly select $t - 1$ integers $a_1, a_2, \ldots, a_{t-1} \bmod p$.
Then let:
$$
  f(x) \= a_0 + a_1x + \ldots + a_{t-1} x^{t-1} \pmod p
$$

Note $f(0) \= a_0 \pmod p$.

- Give each person a distinct ordered pair $(x_i, y_i)$ such that
  $y_i \= f(x_i) \pmod p$.
- For example, we may give out the pairs
  $(1, f(1)), (2, f(2)), \ldots, (w, f(w))$.
- Now suppose $t$ people share their pairs.
- How can $a_0$ be recovered?

**Example:**
- $p = 17$
- $t = 3$
- $w = 5$

$$
  f(x) = a_0 + a_1x + a_2x^2
$$

Person $i$ gets point $P_i$.
$$\begin{aligned}
  P_1 &= (1,  8) \\
  P_2 &= (2,  7) \\
  P_3 &= (3, 10) \\
  P_4 &= (4,  0) \\
  P_5 &= (5, 11)
\end{aligned}$$

Recover the secret using three shares:

Let's say Persons 1, 3, and 5 collaborate.
We now know 3 points of a quadratic equation.
We can solve the system of equations.
All in $\Z_{17}$:
$$\begin{alignat}{3}
  f(1) &= a_0 +  a_1 +    a_2 &&=  8 & \quad (1) \\
  f(3) &= a_0 + 3a_1 +  9 a_2 &&= 10 & (2)       \\
  f(5) &= a_0 + 5a_1 + 25 a_2 &&= 11 & (3)       \\
  (2) - (1) &= 2 a_1 +  8 a_2 &&= 2 & \quad (4)  \\
  (3) - (2) &= 2 a_1 + 16 a_2 &&= 1 & (5)        \\
  (5) - (4) &=          8 a_2 &&= -1             \\
\end{alignat}$$

$$\begin{aligned}
  8 a_2 &\= -1 &\pmod{17} \\
  8 a_2 &\= 16 &\pmod{17} \\
  a_2 &= 2 \\
\end{aligned}$$

Plugging this into (4):
$$\begin{aligned}
  2 a_1 + 8(2) &= 2 \\
  a_1 + 8 &= 1 \\
  a_1 &\= -7 \\
  a_1 &= 10
\end{aligned}$$

Plugging this into (1):
$$\begin{aligned}
  a_0 + 10 + 2 &\= 8 \\
  a_0 + 12 &\= 8 \\
  a_0 &\= -4 \\
  a_0 &= 13
\end{aligned}$$

Therefore:
$$
  f(x) = 13 + 10x + 2x^2
$$

Note that $a_0$ is the secret.

## Existence an uniqueness of solution

Suppose we have a polynomial $f(x)$ of degree $t - 1$ that we would like to
reconstruct from the pairs $(x_1, y_1), \ldots, (x_t, y_t)$.
This means:
$$
  y_k \= a_0 + a_1 x_k + \ldots + a_{t-1}{x_k}^{t-1} \pmod p
  \textnormal{ for } 1 \le k \le t
$$

As a matrix equation:
$$
  \begin{bmatrix}
    1 & x_1 & \cdots & {x_1}^{t-1} \\
    1 & x_2 & \cdots & {x_1}^{t-1} \\
    \vdots & \vdots & \vdots & \vdots \\
    1 & x_t & \cdots & {x_1}^{t-1}
  \end{bmatrix}
  \begin{bmatrix}
    a_0 \\
    a_1 \\
    \vdots \\
    a_{t-1}
  \end{bmatrix}
  \=
  \begin{bmatrix}
    y_1 \\
    y_1 \\
    \vdots \\
    y_t \\
  \end{bmatrix}
$$

The matrix $V$ is the Vandermonde matrix.
It can be shown that the determinant is:
$$
  \det V = \prod_{1 \le j < k \le t} (x_k - x_j)
$$

Which is zero mod $p$ iff two of the $x_i$'s coincide mod $p$.
As long as we use distinct $x_i$'s the system has a unique solution, and any
subset of size $t$ can be used to determine the secret.

In our example:
$$\begin{aligned}
  V &= \begin{bmatrix}
    1 & 1 & 1 \\
    1 & 3 & 9 \\
    1 & 5 & 25
  \end{bmatrix}
  \\
  \det(V) &= (3 - 1) (5 - 1) (5 - 3) \\
  &= 2 (4) (2) \\
  &= 16 \\
  \overrightharpoon{a} &= V^{-1} \cdot \overrightharpoon{y}
\end{aligned}$$

## Lagrange Interpolation

Here is another approach for reconstructing the polynomial.
First we come up with a polynomial for $l_k(x)$ that is the characteristic
function for that $x_k$, namely:
$$\begin{aligned}
  l_k(x_j) &= \begin{cases}
    1 : k = j \\
    0 : k \ne j
  \end{cases} \\
  l_k(x) &= \prod_{i=1, i \ne k}^t \frac{x - x_i}{x_k - x_i} \bmod p
\end{aligned}$$

The Lagrange interpolation polynomial
$$
  f(x) = \sum_{k=1}^t y_k l_k (x)
$$
satisfies the requirement $f(x_j) = y_j$ for $1 \le j \le t$.
For example:
$$\begin{aligned}
  f(x_1) &= y_1 \cdot l_1(x_1) + y_2 \cdot l_2(x_1) + \ldots \\
  &\= y_1 \cdot 1 + y_2 + 0 + \ldots &\pmod p \\
  &\= y_1 &\pmod p
\end{aligned}$$

By uniqueness of solution, $f$ is the unique polynomial of degree $t - 1$
satisfying the $t$ given points.
Then $f(0)$ is the secret.

## Info Revealed by fewer than $t$ shares?
**Example:**
Returning to our above example of a Shamir $(3, 5)$ scheme:

What if two people share their information?

For any $(0, c), c \in \Z_{17}$, there is a degree two polynomial fitting the
three points.
So no information was obtained.
