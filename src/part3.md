# Fermat's Little Theorem

## Reducing expressions in $\Z_n$ computations

Recall:
- When doing computations mod $n$, we may reduce terms in a sum or factors in a
  product without affecting the result.
- For example, in $\Z_{10}$:
  $$
    25 + 37 = 62 = 2 \\
    5 + 7 = 12 = 2
  $$
  $$
    25 \cdot 37 = 925 = 5 \\
    5 \cdot 7 = 35 = 5
  $$

  More generally, if $a_1 \= b_1 \pmod n$ and $a_2 \= b_2$ then:
  $$
    a_1 + a_2 \= b_1 + b_2 \pmod n \\
    a_1 a_2 \= b_1 b_2 \pmod n
  $$
- What about exponentiation?  
  **Exercise:** Can we reduce exponents mod 5 in $\Z_5$?  
  Find an example where:
  $$
    a_1 \= b_1 \pmod 5 \\
    a_2 \= b_2 \pmod 5 \\
  $$
  but
  $$
    {a_1}^{a_2} \not\= {a_1}^{b_2} \pmod 5
  $$
  or explain why no such example exists.

  Let:
  $$\begin{aligned}
    a_1 &= b_1 = 2 \\
    a_2 &= 2 \\
    b_2 &= 7 \= 2 \pmod 5
  \end{aligned}$$

  Then:
  $$\begin{aligned}
    2^2 &\mev 2^7 &\pmod 5 \\
    4 &\mev 128 &\pmod 5 \\
    3 &\not\= 4 &\pmod 5
  \end{aligned}$$

  Or, more trivially:
  $$\begin{aligned}
    2^5 &\nev 2^0 &\mod 5 \\
    32 &\nev 1 &\mod 5 \\
  \end{aligned}$$

## Modular Exponentiation

tldr: it's not valid to reduce an exponent mod $n$ when working in $\Z_n$.

Is there a way to reduce exponents?
- Recall that an element $a \in \Z_n$ is invertible iff $\gcd(a, n) = 1$.
- In the case that we are working in $\Z_p$ where $p$ is a prime, this means
  that all elements of $\Z_p$ except for 0 are invertible.
- In general, we let $\Zs{n}$ be the set of invertible elements mod $n$.
  Note: $\Zs{n}$ is a group.
- In the case of prime modulus $\Zsp$, there are precisely $p - 1$ elements in
  $\Zsp$.

**Example:**
Let's write out the exponentiation table in $\Zs7$:

| $a^0$ | $a^1$ | $a^2$ | $a^3$ | $a^4$ | $a^5$ | $a^6$ |
|-------|-------|-------|-------|-------|-------|-------|
|   1   |   2   |   4   |   1   |   2   |   4   |   1   |
|   1   |   3   |   2   |   6   |   4   |   5   |   1   |
|   1   |   4   |   2   |   1   |   4   |   2   |   1   |
|   1   |   5   |   4   |   6   |   2   |   3   |   1   |
|   1   |   6   |   1   |   6   |   1   |   6   |   1   |

Observations about the table:
- Column of all 1s in $a^6$.
  This is the first (non-trivial) such column.
- In $a^2$ and $a^4$ column 2s and 4s alternate, except when $a = 6$.
- Rows 3 and 5 have every group element.
  These are the only such rows.
- First 1 in any row always appears at a divisor of 6.

## Fermat's Little Theorem

Let $p$ be prime.
If $\gcd(a, p) = 1$, then $a^{p-1} \= 1 \pmod p$.
- Note that FLT says that reduction with respect to the modulus doesn't respect
  exponentiation the way that it does with other operations.
- This can be very powerful.
  For example, show how to find $7^{1002} \bmod 11$ by hand despite the fact
  that $7^{1002}$ is a massive number:

  $$\begin{aligned}
    7^{10} &\= 1 \pmod{11} &\textnormal{by FLT} \\
    7^{1002} &\= \paren{7^{10}}^{100} \cdot 7^2 &\pmod{11} \\
    &\= 1^{100} \cdot 7^2 &\pmod{11} \\
    &\= 7^2 &\pmod{11} \\
    &\= 49 &\pmod{11} \\
    &\= 5 &\pmod{11}
  \end{aligned}$$

- More generally, for any $a, k \in \Z$ and prime $p$, if we have:
  $$
    a^{k(p-1)} \= 1 \pmod p
  $$
  Thus, we can subtract any such multiple from an exponent without changing the
  expression.
- I.e., we can _reduce exponents_ mod $p - 1$ when working in $\Zsp$.

## Orders of Group elements

Let $a \in G$ where $G$ is a group under multiplication.
We say that $\ord_G(a) = k$ (read: "the order of $a$ in $G$ is $k$") if:

- $a^k = 1$
- It is not the case that $a^m = 1$ for any $1 \le m < k$.

I.e., $k$ is the least positive power of $a$ that yields the identity, if such a
power exists.

If no such power exists, we say $a$ has infinite order.

In a finite group, eery element has finite order.

When our group is $\Zs{n}$, we write the order of $a$ in $\Zs{n}$ as
$\ord_n(a)$.

**Example:**

- $\ord_7(2) = 3$
- $\ord_7(3) = 6$
- $\ord_7(4) = 2$ since $4^2 \= 1 \pmod{15}$ and $4^1 \nev 1 \pmod{15}$.

Question: What is the order of the identity in any group?

$$\begin{aligned}
  \ord_G(e) &= 1 \\
  e^1 &= e
\end{aligned}$$


# Lagrange's Theorem

(Really a special case of Lagrange is all we need for this course.
Stated without proof here.)

> **Theorem:**
> The order of any element in a finite group is finite, and divides the size of
> the group.

Fermat's Little Theorem (FLT) follows from Lagrange's Theorem.
Explanation:

Recall $\Zsp$ is a group, and $\abs{\Zsp} = p - 1$.
Let:
$$\begin{aligned}
  a &\in \Zsp \\
  k &= \ord_p(a)
\end{aligned}$$
Then:
$$\begin{aligned}
  a^k &\= 1                 &&(\bmod\ p)                 \\
  k &\mid (p - 1)           &&\tn{by Lagrange's Theorem} \\
  mk &= p - 1               &&\tn{for some } m \in \Z_p  \\
  a^{p-1} &\= a^{mk}                                     \\
  a^{p-1} &\= \paren{a^k}^m                              \\
  a^{p-1} &\= 1^m                                        \\
  a^{p-1} &\= 1
\end{aligned}$$

## cyclic groups

Yes, it is cyclic. 3 and 5 are cyclic.
