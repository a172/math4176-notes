# Part 1

Notation:

- $\mathbb{N}$: Naturals (including 0) <!-- \N \natnums -->
- $\mathbb{Z}$: Integers               <!-- \Z          -->
- $\mathbb{Q}$: Rational               <!--             -->
- $\mathbb{R}$: Real                   <!-- \R \reals   -->
- $\mathbb{C}$: Complex                <!--    \cnums   -->

When written with a star, such as $\Z^\ast$, this indicates only the nonzero
elements of the set.

## Some Basic Group Theory

The notion of a mathematical structure called a _group_ will be essential for
us.
There are many familiar examples of groups from elementary mathematics.
A group is a set $G$ together with a binary operation (usually denoted $\cdot$
or $+$ depending on the context) such that:

1. For all $x, y, z \in G$ we have $(x \cdot y) \cdot z = x \cdot (y \cdot z)$
  (Associativity)
1. There is an element $e \in G$ such that
  $e \cdot x = x \cdot e = x$ for all $x \in G$.
  (Identity)
1. For each $x \in G$, there is an element $y$ such that
  $x \cdot y = y \cdot x = e$.
  (Inverse)

Comments:

1. Groups where the operation is multiplication (in some sense) are called
   multiplicative groups and the group operation is usually denoted as $\cdot$
   (or just juxtaposition).
1. Groups where the operationis addition (in some sense) are called additive
   groups and the group operation is usually denoted as $+$.
1. The identity is usually denoted 1 for multiplicative group, 0 for an additive
   group.
1. The inverse of $x$ is usually denoted as $x^{-1}$ when the group operation is
   written multiplicatively, and $-x$ when it is written additively.
   We will mostly use the former notation.
1. The group operation is not always commutative.
   Commutative groups are also known as Abelian groups.
   We will almost exclusively be using Abelian groups.
1. The definition of a group must include the operation.
   A set may or may not constitute a group depending on the chosen operation.

## Examples

- $\Z$ using $+$
  - Associative: yes
  - Identity: $0 + x = x + 0 = x$
  - Inverse: $x + (-x) = (-x) + x = 0$
  - IS a group
- $\Z$ using $\cdot$
  - Associative: yes
  - Identity: $1x = x1 = x$
  - Inverse:
      No.
      We need rationals to have inverses.
  - IS NOT a group
- Each of $\mathbb{Q}$, $\R$, and $\cnums$
  - using $+$
    - Is a group for the same reasons as $\Z$ with $+$.
  - using $\cdot$
    - Is NOT a group, because 0 has no inverse.
- $\N$ using each of $+$ and $\cdot$
  - using $+$
    - No.
      We need negatives to have inverses.
  - using $\cdot$
    - No.
      We need rationals to have inverses.
- $\mathbb{Q}^\ast$
  - using $+$
    - This does not have closure, so it is not a group.
      Example:
      $$ 5 + -5 = 0$$
  - using $\cdot$
    - Associative: yes
    - Identity: 1
    - Inverse: $x \cdot x^{-1} = 1$
    - IS a group

## Some Basic Number Theory

All variables stand for integers in that follows.

- Divisibility.
  $n \mid a$ if there is an integer $k$ such that $a = nk$.
  If $n \mid a$ and $n \mid b$, then for any $x, y$ we have:
  $$ n \mid (ax + by) $$

  Proof:

  Can write $a=nk$ and $b=nl$ for some $k,l \in \Z$, then
  $$ax + by = nkx + nly = n(kx + ly)$$

  Thus, $n \mid (ax + by)$

- Congruence mod $n$.
  We say $a \equiv b \pmod n$ if $n$ evenly divides $a-b$.

  $$\begin{aligned}
    23 &\equiv 73 \pmod{10} &\because 23 - 73 = -50;\; 10 \mid -50 \\
    22 &\not\equiv 86 \pmod{10} &\because 11 - 86 = -75;\; 10 \nmid -75
  \end{aligned}$$

- We can use the symbol "mod" as an operator, and say $y = a \bmod n$
  iff $a \equiv y \pmod n$ and $0 \le y < n$.
  Then $y$ is called the residue of $a$ in $\Z_n$.
  $$ 73 \bmod 10 = 3 $$

## Modular Arithmetic

- Define $\Z_n = \{0,\, 1,\, 2,\, \ldots,\, n - 1\}$.
- For example, $\Z_6 = \{0,\, 1,\, 2,\, 3,\, 4,\, 5 \}$.
- Note that every integer is congruent to exactly one element in the set.
  In some sense, e.g., $ 1 \in \Z_6$ is standing in for the entire set
  $\{\ldots,\, -5,\, 1,\, 7,\, \ldots\}$
- This is an example of an algebraic structure called a _ring_ (meaning we have
  additive and multiplicative operations).
  We can define addition and multiplication on $\Z_n$ as:
  - $a +_{\Z_n} b = a + b \bmod n$
  - $a \cdot_{\Z_n} b = a \cdot b \bmod n$

  Example (in $Z_7$):
  $$\begin{aligned}
    4 +_{\Z_n} 5 &= 4 + 5 \bmod 7 \\
    &= 9 \bmod 7 \\
    &= 2 \\
  \end{aligned}$$
  $$\begin{aligned}
    4 \cdot_{\Z_n} 5 &= 4 \cdot 5 \bmod 7 \\
    &= 20 \bmod 7 \\
    &= 6
  \end{aligned}$$

- The subscript notation (on the operators) is usually understood from context
  and suppressed.
- Whenever inputs are replaced by numbers equivalent mod $n$, the resulting
  product or sum is the same.

  Example (in $Z_7$):
  $$\begin{aligned}
    4 + 12 &\equiv 4 + 5 \\
    &\equiv 2 \\
  \end{aligned}$$
  $$\begin{aligned}
    4 \cdot 12 &\equiv 4 \cdot 5 \\
    &\equiv 6
  \end{aligned}$$

## Invertiblity

- Not every element in a ring has a multiplicative inverse.
- An element $b \in Z_n$ has a multiplicative inverse (or is simply invertible)
  provided that $\exists c \in \Z_n$ such that $bc \equiv 1 \pmod n$.
- For example, $5 \cdot 9 = 45 \equiv 1 \pmod{11}$, so 5 and 9 are invertible
  mod 11.
- Another example, 4 is not invertible mod 12.
  Why not?

  $4x \bmod 12 = 4x - 12y$ for some $x, y \in \Z$.  
  But $4 \mid (4x - 12y)$, and $4 \nmid 1$.  
  Thus $4x \not\equiv 1 \pmod{12}$.

We let $\Z_n^\ast$ denote the set of invertible elements in $\Z_n$.
For example:
$$\begin{aligned}
  \Z_5^\ast &= \{1,\, 2,\, 3,\, 4\} \\
  \Z_{10}^\ast &= \{1,\, 3,\, 7,\, 9\}
\end{aligned}$$

Note that both are groups under $\cdot$.

### GCD
Let $a, b$ be integers, not both zero.
We say that $d = \gcd(a, b)$ if:
- $d \mid a$ and $d \mid b$
- For any $c$ such that $c \mid a$ and $c \mid b$ we have $c \mid d$
- $d > 0$

### Condition for Invertiblity
We'll be interested in knowing when a $b \in \Z_n$ is invertible.
We'll see that $b$ is invertible iff $\gcd(b, n) = 1$.
That is, $b$ and $n$ are coprime or relatively prime.

Therefore, we shall like to compute GCDs efficiently.
