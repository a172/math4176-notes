# Part 23

## Undeniable Signatures

_Undeniable signature schemes_ were introduced by Chaum an dvan Antwerpen in
1989.

- Signatures cannot be verified without cooperation of Alice.
- This protects Alice against the possibility that documents signed by her are
  duplicated and distributed without her approval.
- For example, if Alice only wants persons of her choosing to be able to verify
  signatures, and not the world at large, this may be useful.
- Done by means of a _challenge-and-response protocol_
- Alice's cooperation is required to verify a signature, but what's the prevent
  disavowal?
- it seems like signatures are useless to intended recipients unless Alice can
  be compelled to verify them.
- To overcome this problem, an undeniable signature scheme incorporates a
  _disavowal protocol_ by which Alice can prove a signature is a forgery.
- Alice would be able to prove "in court" that a forged signature is, in fact, a
  forgery.
- If she refuses, this would be regarded as evidence that the signature is in
  face genuine.

## Chaum-van Antwerpen Signature Scheme

### Setup

Alice sets up the following:
- Primes $p$ and $q$ such that $p = 2q + 1$.
- An element $\alpha \in \Zs{p}$ of order $q$.
- An exponent $1 < a < q$ and $\beta = \alpha^a \bmod p$.
- Publish $p, \alpha, \beta$, keep $a$ private.

### A note on notation

Let $G$ denote the multiplicative subgroup $\ang{\alpha}$, (note $\abs{G} = q$).

Below we will work in $G$ using the notation for a general group, without the
mod notation.

Since the order of $\alpha$ is $q$, when we work in exponents, all arithmetic
will be done mod $q$.

### The Signature

For a message $x \in G$, define $y = x^a \bmod p$.
Then $(x, y)$ is the signed message.

### Verification
1. Bob chooses $e_1, e_2$ at random, $e_1, e_2 \in \Z_q$
1. Bob computes and sends to Alice:
   $$
     c = y^{e_1} \beta^{e_2} \bmod p
   $$
1. Alice computes and sends to Bob:
   $$
     d = c^{a^{-1} \bmod q} \bmod p
   $$
1. Bob accepts $(x, y)$ as valid iff $d \= x^{e_1} \alpha^{e_2} \mod p$.
   $$\begin{aligned}
     c^{a^{-1}} &\= \paren{y^{e_1} \beta^{e^2}}^{a^{-1}} \\
     &\= \paren{x^{a e_1} \alpha^{ae_2}}^{a^{-1}} \\
     &\= \paren{x^{ae_1} \ldots} \ldots
   \end{aligned}$$

### An example
- Let $p = 59$.
- Then 2 is a primitive element, so we can take $\alpha = 2^2 = 4$, which has
  order 29.
- Let $a = 10$, then $\beta \= \alpha^{10} \= 28 \pmod{59}$.
- Alice wants to sign the message $x = 41$, so she publishes $x$ and the
  signature $y \= 41^{40} \= 35 \pmod{59}$.
  The signed message is then $(41, 35)$.

Note that $a^{-1} \= 3 \pmod p$.

- Bob asks Alice to verify the signature to him.
- He chooses $(e_1, e_2) = (5, 9)$:
<!-- TODO: cleanup -->
- Challenge:
  $$\begin{aligned}
    c &\= y^{e_1} \beta^{e_2} \\
    &\= 35^5 \cdot 28^9 \\
    &\= 21 \pmod{59}
  \end{aligned}$$
- $B \to A$ (c = 21)
- A cmputes to $c^{a-1} \= 21^3 \= 57 \pmod{59}$
- $A \to B$ ($d= 57$)
- B verifies $x^{e_1} \alpha^{e_2} \= 41^5 \cdot 4^9 \= 57 \pmod{59}$

$$\begin{aligned}
\end{aligned}$$

## Security against Fraudulent Signatures
- We now show that Alice can't fool Bob into accepting a fraudulent signature,
  except with very small probability.
- Since Alice has more information than anyone else, this means that no one else
  will be able to fool Bob, either.
- However, it's important that Alice not be able to do so, because unlike
  previous signature schemes we looked at she may have motive to do so.
  (Alice publishes fraudulent signature, so she is able to deny it later; she
  shouldn't be able to confirm it as well.)

**Theorem:**
If $y \not\= x^a \pmod p$, then any response given by Alice to Bob's challenge
will be accepted with probability at most $\frac1q$.

**Proof:**

First, given challenge $c$, we'll count the number of pairs $(e_1, e_2)$ Bob
could have used to generate $c$.
$$
  c \= y^{e_1} \beta^{e_2}
$$

There are $q$ choices for $e_1$, because is must be an element in $\Z_q$.
Given $e_1$, we need $\beta^{e_2} \= c \cdot y^{-e_1}$.
We know $\abs{G} = q$, which is prime.
So $\ord{\beta} = q$, since $\beta \ne 1$.
So there is a unique $e_2 \in Z_q$ that satisfies the congruence.
Thus, there are $q$ pairs that could have produce $c$.

Now, since $G = \ang{\alpha}$, we can write:
$$\begin{aligned}
  c &= \alpha^i \\
  d &= \alpha^j \\
  x &= \alpha^k \\
  y &= \alpha^l
\end{aligned}$$

Also note:
$$\begin{aligned}
  y \not\= x^a \\
  \alpha^l \not\= \alpha^{ak}
\end{aligned}$$

If challenge were accepted, we'd have:
$$\begin{aligned}
  c &\= y^{e_1} \beta^{e_2} &\pmod p \\
  d &\= x^{e_1} \alpha^{e_2} &\pmod p \\
  i &\= le_1 + ae_2 &\pmod q \\
  j &\= ke_1 + e_2 &\pmod q
\end{aligned}$$

This lets us write:
$$\begin{aligned}
  \begin{bmatrix}
    l & a \\
    k & 1
  \end{bmatrix}
  \begin{bmatrix}
    e_1 \\
    e_2
  \end{bmatrix}
  &\=
  \begin{bmatrix}
    i \\
    j
  \end{bmatrix}
  & \pmod q
\end{aligned}$$

Note $\det
  \begin{bmatrix}
    l & a \\
    k & 1
  \end{bmatrix}
  \= l - ak \not\= 0 \pmod q$
since $l - ak \= 0 \iff l \= ak \pmod q \iff y \= x^a \pmod p$.

So, for every $\begin{bmatrix}e_1 \\ e_2\end{bmatrix}$ there is exactly one
$\begin{bmatrix}i \\j \end{bmatrix}$ that verifies, since $d \= \alpha^j \pmod
p$ there is exactly one $d$ that verifies.

## Disavowal Protocol

To disavow a message, Alice is given a challenge $c$ by Bob and has to produce
$d$ which is not verifiable as the correct response.
This appears simple, but Alice has to respond "incorrectly", and in such a way
that the responses are consistent with each other (and with her following the
protocol).

1. Bob chooses $e_1, e_2 \in \Zsp$ at random.
1. Bob computes $c = y^{e_1} \beta^{e_2}$ and sends it to Alice.
1. Alice computes $d = c^{a^{-1}}$ and sends it to Bob.
1. Bob verifies that $d \not\equiv x^{e_1} \alpha^{e_2}$
1. Bob chooses $f_1, f_2$ at random $f_1, f_2 \in \Zsp$.
1. Bob computes $C = y^{f_1} \beta^{f_2}$
1. Alice computes $D = C^{a^{-1}}$
1. Bob verifies $D \not\= x^{f_1} \alpha^{f_2}$
1. Bob concludes that $y$ is a forgery if and only if
  $$ \paren{d \alpha^{-e_2}}^{f_1} \= \paren{D \alpha^{-f_2}}^{e_1} \pmod p $$

**Proof:**

Assuming protocol is followed:
- Define $z \= y^{a^{-1}}$
- If $(x, y)$ is invalid then $x \ne z$.

We have:
$$\begin{aligned}
  d &= c^{a^{-1}} \\
  &= z^{e_1} \alpha^{e_2} \\
  D &= C^{a^{-1}} \\
  &= z^{f_1} \alpha^{f_2}
\end{aligned}$$


$$\begin{aligned}
  z^{e_1} &= d \cdot \alpha^{-e_2} \\
  z^{f_1} &= D \cdot \alpha^{-f_2}
\end{aligned}$$

$$\begin{aligned}
  \paren{z^{e_1}}^{f_1} &= \paren{d \cdot \alpha^{-e_2}}^{f_1} \\
  \paren{z^{f_1}}^{e_1} &= \paren{D \cdot \alpha^{-f_2}}^{e_1} \\
\end{aligned}$$

**Theorem:**
Suppose $y \= x^a \pmod p$ (implies $(x, y)$ is valid) and Bob follows the
disavowal protocol.
If $d \= x^{e_1} \alpha^{e_2} \pmod p$ and $D \not\= x^{f_1} \alpha^{f_2} \pmod
p$, then the following holds with probability $1 - \frac1q$:
$$
  \paren{d \alpha^{-e_2}}^{f_1} \not\= \paren{D \alpha^{-f_2}}^{e_1} \pmod p
$$

**Proof:**
Assume we have:
$$\begin{aligned}
  \paren{d \alpha^{-e_2}}^{f_1} &\= \paren{D \alpha^{-f_2}}^{e_1} \pmod p \\
%
  \paren{d^{{e_1}^{-1}} \alpha^{-e_2 \cdot e_1^{-1}}}^{f_1}
  \cdot \alpha^{f_2} &\equiv D \pmod p \\
%
  d_0 &= d^{{e_1}^{-1}} \alpha^{-e_2 \cdot e_1^{-1}} &\text{define this} \\
  d_0^{f_1} \cdot \alpha^{f_2} &\= D \pmod p
\end{aligned}$$

This says response to $C$ has to be done as if the signed message was
$(d_0, y)$.

If this is not valid (i.e., $y \not\= \paren{d_0}^a$), succeeds with probability
$\frac1q$.
It's valid iff $d_0 = x$.

Does $d_0 = x$?
From second non-congruence, we have:
$$\begin{aligned}
  x \not\= \paren{d \cdot \alpha^{-e_2}}^{{e_1}^-1} = d_0
\end{aligned}$$
