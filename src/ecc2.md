# ECC part 2

$$
  E = \{(x, y) : y^2 = x^3 + 73 \}
$$

## Adding points algebraically

- Use the given points to find the line in the form of $y = mx + b$.
- Use this value of $y$ in the definition of $E$ to get a cubic function
- Note that this cubic function is equal to $(x - x_1)(x - x_2) (x - x_3)$ with
  roots $(x_1, x_2, x_3)$
- This gives us $x_3$, which we can plug into our line to find $y_3'$, and $y_3
  = -y_3'$

## Adding a point to itself

- First, we need the equation of the tangent line to E at $R = (x_1, y_1) = (-4,
  -3)$
- Use implicit differentiation (ugh)

$$
  y^2 = x^3 + 73 \\
  2y \times y' = 3x^2
$$
implies
$$
  y' = \frac{3x^2}{2y}
  y' |_ {(-4, -3)} = \frac{48}{-6} = -8
$$

An equation for tangent line is
$$
  (y - (-3)) = -8 (x - (-4)) \\
  y = -8x - 35
$$

Plug into E:
$$
  (-8x - 35)^2 = x^3 + 73 \\
  0 = x^3 - 64x^2 + \ldots \\
  = (x - x_3) (x - x_3) (x - x_4) \tn{note double root} \\
  (x_3 + x_3 + x_4 = 64 \\
  x_4 = 72
$$

So, 
$$
-y_4 = -8 \times 72 - 35 \\
= -611
y4 = 611
R + R = S = (72, 611)
$$

## General Formulas

$$
  P = (x_1, y_1) \\
  Q = (x_2, y_2)
$$

Cases:
1. 0 + P = P
2. P + (-P) = (x_1, y_1) + (x_1, -y_1) = 0
3. $x_1 \ne x_2$ where $(P, Q \ne 0)$
  Let $y = mx + c$ be an weation for line between $P$ and $Q$.
  Then $m = \frac{y_2 - y_1}{x_2 - x_1}$
  $$
    (mx + c)^2 = x^3 + ax + b \\
    0 = x^3 - m^2x^2 + \ldots
  $$
  So:
  $$
    c_2 = -m^2 \\
    x_3 = m^2 - x_1 - x_2
  $$
  Now use slope between $P$ and $R'$ is $m$ to get:
  $$
    m = \frac{-y_3 - y_1}{x_3 - x_1} \\
     y_3 = m(x_1 - x_3) - y_1
  $$

  Summary:
  - $m = \frac{y_2 - y_1}{x_2 - x_1}$
  - $x_3 = m^2 - x_1 - x_2$
  - $y_3 = m (x_1 - x_3) - y_1$
4. $P = Q$
  ($a$ from differentiation)
  $$
    2y \times y' = 3x^2 + a \\
    y' = \frac{3x^2 + a}{2y}
  $$
  Then:
  - $m = \frac{3x_1^2 + a}{2 y_1}$
  - $x_3 = m^2 - x_1 - x_2$
  - $y_3 = m (x_1 - x_3) - y_1$

## Moving out of $\R$

$\R$ is an example of a structure called a _field_.
It has $+$, $\times$, with all usual properties, and any nonzero element has a
multiplicative inverse.
$\Z_{17}$ is a (finite) field.
$\Z_{30}$ is _not_ a field.
