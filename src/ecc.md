# Elliptic Curve Cryptography

## Cryptography in Generic Groups

Discrete log based systems can be implemented in any finite group.

Ex:
Diffie-Hellman

A and B agree upon finite group $G$ and $\alpha \in G$.

A picks $x$, B picks $y$.
A computes $g = \alpha^x$.
B computes $h = \alpha^y$.

Exchanging these, $s = h^x = g^y = \alpha^{xy}$.

This does not rely on modular arithmetic.
It does rely on discrete logs in the group being infeasible.

## Motivation

Is there a reason to not just work in $\Zsp$?
- Recall that we had "generic" algorithms for computing discrete logs.
  - Shanks
  - Pollard-Rho
  - These require $\sqrt{n}$ iterations where $n = \ord(\alpha)$.
  - This means they grow exponentially with the bit length.
    That is, they take about exp(m/2) iterations where $m$ is teh bit-length of
    $n$
- The Index Calculus:
  - Not generic
  - Requires notion of a prime number in $G$.
  - Runs much faster, sub-exponential.
    ~exp(\sqrt{m}) iterations.

## Elliptic Curves

### Why?
Find examples of groups with no notion of prime.

### What?
The name comes from elliptic integrals which are used for something else.
The curves themselves are not elliptic, and the relation to ellipsis isn't
actually important.

Sets of form:
$$
  \{ (x, y) : y^2 = x^3 + ax + b \}
$$
(General quadratic to general cubic can always be written in this form.)
Where $4a^3 + 27b^2 \ne 0$. That is, non-singular curves (related to the
smoothness of the curve).

And point:
$$
  \{ (\infty, \infty) \}
$$
Where $(\infty, \infty)$ is "point at infinity".
We can also write $(\infty, \infty) = 0$.

In cryptographic applications, $x, y \in \Z_p$ for some prime p.
It is slightly more general than this.

To gain a geometric intuition for the moment, think of $x, y \in \R$.

Recall a cubic polynomial with all real coefficients, e.g., $x^3 + ax + b$ has
either 1 or 3 real roots, counting multiplicity.
$y^2 = x^3 + ax + b$

Note that the curve has symmetry about the x-axis.
If (x, y) is a point on the curve, so is (x, -y).

### Group definition
We use additive notation.
We an define a group operation on any non-singular elliptic curve E:
- The identity is point at infinity (0).
- If P = (x, y), its inverse is -P = (x, -y).
- The identity is its own inverse.

Addition:
Idea is if P, Q, R' are colinear points on E, P + Q + R' = 0.
Then, P + Q = -R'.
Let R = -R'.
Then P + Q = R.
See [link][ecpa].

Other cases:
- If P = 0 or --> Q = 0 <--
  - P + 0 = P
  - Geometrically: think of _all_ vertical lines as intersecting 0.
  - So P, -P, and 0 are colinear.
  - Then, $P + (-P) + 0 = 0 \implies P + 0 = P$.
- If Q = -P, then:
  - P, -P, 0 are colinear
  - P + (-P) + 0 = 0 \implies P + (-P) = 0
- If P = Q
  - Use the tangent line and consider P as 2 points of intersection.
  - P + P + R' = 0
  - P + P = R
- What if line between P and Q doesn't intersect E in a 3rd point?
  - This can only happen if the line is tangent to E at P or Q (say P).
  - P + P + Q = 0
  - P + Q = -P

[ecpa]: https://andrea.corbellini.name/ecc/interactive/reals-add.html
