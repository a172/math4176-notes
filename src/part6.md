# Part 6

## Primal testing

How do we efficiently find large primes?

### Fermat Compositeness Test

Test: $n = 341$ for compositeness using FCT:

Try "randomization" $b = 2$.

$$
2^{340} \equiv 1 \mod 341
$$

This gives a "yes". But this can be a false positive.

Try "radomization" $b = 3$.

$$
3^{340} \equiv 56 \mod 341
$$

Therefore, we know it is actually composite.

## Randomized Algorithms

Yes-biased with error probability $\epsilon$:
- YES is always correct
- If the correct output is YES, algorithm returns NO with probability $\le
  \epsilon$.
- Therefore, After $k$ trials, the probability of a false negative is
  $\epsilon^k$.

### Fermat Pseudoprimes
Correction from the notes (as of 2021-09-13):
- We call 3 a _Fermat witness_ for 341.
- We call 2 a _Fermat liar_ for 341.


### Carmichael numbers suck.

A Carmichael number is a composite number $n$ such that for all $b$ with
$\gcd(b, n) = 1,\, b^{n-1} \equiv 1 \mod n$.

WTS: $b^{560) \equiv 1 \mod 561$

Note that $561 = 3 \times 11 \times 17$.
If $b \in \Z^\ast_{561}$, $b \in \Z_3, \Z_11, \Z_17$.
Then by FLT:

$$
\begin{aligned}
b^2 &\equiv 1 \mod 3 \\
b^10 &\equiv 1 \mod 11 \\
b^16 &\equiv 1 \mod 17 \\
\\
b^{560} &\equiv (b^2)^{280} \equiv 1 \mod 3 \\
b^{560} &\equiv (b^{10})^{56} \equiv 1 \mod 11 \\
b^{560} &\equiv (b^{16})^{35} \equiv 1 \mod 17 \\
\end{aligned}
$$

By the Chinese Remainder theorem, there is a solution.



