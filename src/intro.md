# Intro

The essential problem is to send a message securely over a non-secure channel.
See Alice and Bob.

- Plain text $\theta$.
- Ciphertext $\gamma$

Keyed cryptosystems:
A and B agree upon algorithm and key K.

$$\begin{align}
  \gamma &= e_K(\theta) \\
  \theta &= e_K^{-1}(\gamma)
\end{align}$$

## Problem with symmetric crypto

If A and B agree upon AES, they need to establish a secure key K.
But how do we establish K over an insecure channel?

## Public Key crypto

Bob creates a key pair (a, b), where a is public and b is private.
Every other party can then use this public key (a) to _encrypt_ a message to
Bob.
Since only Bob has the private key (b), only Bob can decrypt the key.

For example, if Alice wants to send a message to Bob, she can send:

$$\gamma_1 = e_a(\theta_1)$$

Bob then decrypts as:

$$\theta_1 = d_b(\gamma_1)$$

### Pitfalls

- impersonation
- deriving the private key from the public key

### Needs

- $e_a(\theta)$ needs to be efficient to compute
- difficult to invert
- $e_a^{-1}(\gamma)$ is easy to compute iff you know b.

This is done with a trapdoor or one-way function.

### In practice

Asymmetric crypto is much slower than symmetric.
Therefore, Asymmetric crypto is typically used to establish a symmetric key.
