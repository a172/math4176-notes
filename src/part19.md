# Part 19

## A Puzzle

Suppose Alice has a highly accurate system for predicting the results of
football games.
She wants to sell her algorithm to Bob so that he can bet.
Bob wants proof that the algorithm actually works in advance, so he asks Alice
for the predictions for this weekend's games.
Alice refuses, since Bob will be able to bet on the games, make his fortune and
not buy the system.
She offers to show Bob the predictions for last weeks games instead.
Clearly we have a problem.
Is there a solution?

## Finding Bits of a Discrete Log in $\Zsp$

Let $p$ be a prime and $\alpha \in \Zsp$ primitive, so that
$\ord_p(\alpha) = p - 1$.
Suppose we are given $\beta$ and which to find $\log(\beta)$ i.e., $x$ such that:
$$ \alpha^x \equiv \beta \pmod p $$

Question: Can we determine bits of $x$ without necessarily being able to compute
all of $x$? How many?

**Ex:**
Let $p = 13$ and $\alpha = 2$.
Let $x = \log(6)$, so that $2^x \equiv 6 \pmod{13}$.
Find as many bits of $x$ as possible.

## Calculating Discrete Logs

When $p - 1$ factors into powers of small primes, we may use similar ideas to
find the log with respect to each prime power and use the CRT to recombine them.
Details omitted (for now).

## Primes of the form $4n + 3$
- Any odd prime is of the form $p = 4n + 1$ or $p = 4n + 3$ for some $n \ge 0$
- For the former, if $x = \log_\alpha(\beta) \in \Zsp$
- What about primes of the form $4n + 3$? We can find $x_0$.
  Is there an alternate method that would allow us to find $x_1$?
- Note that knowing the least two significant bits is the same as knowing the
  value of $x \bmod 4$.

## Maybe Not?

There is a reason why we would expect an efficient algorithm not to exist.
A natural point of view is that a discrete log should be regarded as a number
mod $p - 1$.
- If $p = 4n + 1$, and if $\alpha^x \equiv \alpha^y \pmod p$, then
  $x \equiv y \pmod{4n}$ and so $x \equiv y \pmod 4$.
  So talking about $\log(\beta) \bmod 4$ makes sense, even if we don't insist
  that it be normalized to be in $\{0, p - 2\}$.
- On the other hand if $p = 4n + 3$, if we used a different normalization, we
  could get a different value for $\log{\beta} \bmod 4$.
- For example, if $p = 11$
  $$ 9 \equiv 2^6 \equiv 2^{16} \pmod{11} $$
  In this case $\log_2^{11}(9) = 6$ but if we allowed to take on values out side
  of the specified range, we could write $\log_2^{11}(9) = 16$.

## Reducing DLP to finding $x_1$ in $4n + 3$ case

**Lemma:**
Let $p \equiv 3 \pmod 4$ be prime, and $m$ be an integer.
Suppose $\alpha$ and $\gamma$ are two nonzero numbers mod $p$ such that
$\gamma \equiv \alpha^{4m} \pmod p$.
Then
$$ \gamma^{frac{p+1}4} \equiv \alpha^{2m} \pmod p $$

**Proof:**



- Now assume $\alpha^x \equiv \beta \pmod p$, where $p \equiv 3 \pmod 4$.
  Assume we have a machine that given input $\beta$ gives output
  $\log(\beta) \bmod 4$.
- We use the machine to find $x_0$ and $x_1$. Then:




## Bit Commitment

We now return to original problem.
Alice wants to send a bit $b$ to Bob.
There are two requirements:
1. Bob cannot determine the value of the bit without Alice's help
2. Alice cannot change the bit once she sends it

Alice puts the bit in a box, puts her lock on it, and sends it to Bob.
When Bob wants the value of the bit, Alice removes her lock and Bob opens the
box.
Is there a mathematical implementation?

Here is the bit commitment protocol:

- Alice and Bob agree upon a large prime $p \equiv 3 \pmod 4$ and a primitive
  root $\alpha$.
- Alice chooses a random $1 < x < p - 1$ whose second bit is $b$.
  Ie, $x = x_0 + 2x_1 + 4x_2 + \ldots$, where $x_1 = b$.
- Alice computes $\beta \equiv \alpha^x \pmod p$, and sends $\beta$ Bob.
- When Bob wants to know $b$, Alice sends him the full value of $x$
- Bob checks that $\beta \equiv \alpha^x \pmod p$.
  Bob infers that $b = x_1$.
- Since the congruence has unique solution for $x$ in the given range, Alice
  cannot send a different value for $x$ than the one she used to generate
  $\beta$.
  The bit $b = x_1$ is determined uniquely.

**Ex:**
- Alice and Bob agree upon $p = 523$ and $\alpha = 2$.
- Alice wants to predict that team $X$ will win this weekend.
- Alice and Bob have agreed that bit 1 signifies win, 0 a loss.
- Alice chooses secret $x$, with 1 in the two's position, (i.e., either $x
  \equiv 2 \pmod 4$ or $x \equiv 3 \pmod 4$), calculates
  $\beta \equiv \alpha^x \pmod p$
- It turns out $\beta = 248$ and sends the pair (Team X, 248).
- Note Bob can compute $x_0 = 1$, since $248^{261} \equiv -1 \pmod p$.
- Team $X$ winds over the weekend and Alice sends Bob $x = 107$.
- Bob verifies $248 \equiv 2^{107} \pmod p$.
- Checks that $x \equiv 3 \pmod 4$, (in fact $x = 1101011_2).
