# Part 17

**2021-10-18**

## Non-invertible coefficients

**Ex**
Suppose we want to find log_\aplha(\beta) in \Z_1010^\ast, where \alpha =4 ...
$$ 18x \equiv 10 \pmod{50}$$

How can we solve the congruence? What is the correct value of $x$?

$$\begin{aligned}
  50 \mid (15x - 10) &\implies 10 \mid (3x-2) \\
  &\implies 3x \equiv 2 \pmod{10} \\
  3^{-1} \equiv 7 \pmod{10} &\implies x \equiv 14 \pmod{10} \\
  &equiv \pmod{10}
\end{aligned}$$

Solutions to $15x \equiv 10 \pmod{50}$ are:
$$ x = 4, 14, 24, 34, 44 $$

Check $\alpha^{24} = \beta$.


General method for solving linear congruences:

To solve $ax \equiv b \pmod n$, a non-invertible mod $n$, let $d = \gcd(a, n)$.
- if \(d \not\div b\), no solutions.
- otherwise, solutions are the same as those of:
  \[ (a/d)x \equiv b/d \pmod (n/d) \]
  This can be solved as:
  \[ x \equiv (a/d)^-1 (b/d) \pmod (n/d) \]
- we get \(d\) total solutions to original congruence:
  \[x, x + (n/d), x + d(n/d), ..., x + (d-1)(n/d) \]

## Additional Exercises

Find any and all solutions to the following congruences:

1. $21x \equiv 35 \pmod{91}$
  $$\begin{aligned}
    \ldots
    3^{-1} \equiv 9 \pmod{13}
  \end{aligned}$$

  $$\begin{aligned}
    3x &\equiv 5 \pmod{13} \\
    x &\equiv 45 \pmod{13} \\
    &\equiv 6 \pmod{13}
  \end{aligned}$$

  All solutions mod 91:
  $$ x = 6, 19, 32, 45, 58, 71, 84$$

2. 18x equiv 33 pmod 225
  $$\begin{aligned}
    \gcd(18, 225) = 9 \\
    \ldots
  \end{aligned}$$
  No solutions.
