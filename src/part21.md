# Part 21

## Digital Signatures

- If Bob sets up an instance of RSA, anyone can send a message to Bob.
- How can Bob verify that a message is actually from its purported sender?
- _Digital Signatures_ aid with message authentication.
- Since a digital signature is just a sequence of bits, it can be easily
  reproduced.
- Therefore to have security against forgeries, we need signatures to depend on
  the _message_ as well as the sender.
- A digital signature scheme consists of:
  1. A method by which a user (Alice) produces a signature $y$ on a message $m$.
  1. A method by which recipients can verify that $y$ is a valid signature on
     $m$ for user Alice.
- We will look at _public key_ signature schemes, in which Alice sets up and
  publishes (some) parameters that will be used to verify the signature.
- The method should be resistant to _forgeries_ - signatures produced by users
  other than Alice.

## RSA signatures

Set up:
- Alice generates two large primes $p, q$, $p \ne q$, and computes $n = pq$.
  She chooses $b \in \Zs{\phi(n)}$, and calculates
  $a \equiv b^{-1} \pmod{\phi(n)}$.
  Alice publishes $(b, n)$ and keeps $(a, p, q)$ private.

Signature:
- Alice's signature is $y = m^a \bmod n$
- Alice sends pair $(m, y)$.
  (This is basically the "decryption" of $m$ in this instance of RSA.)

Verification:
- Bob downloads $(b, n)$
- Computes $z = y^b \bmod n$.
  If $z = m$, then the signature is valid.
- $z = y^b \bmod n$.
  If $z = m$, then the signature is valid.
  $$\begin{aligned}
    z \equiv y^b \equiv \paren{m^a}^b \equiv m^{ab} \equiv m^{ab \bmod \phi(n)}
    \equiv m^1 \equiv m \pmod n
  \end{aligned}$$

## Forgeries

Two types of forgeries:
1. Selective forgery: Forger can produce a valid signature for any given message
   $x$ with high probability.
1. Existential forgery: Forger can produce a valid signature for _some_ message,
   but has no control over the message.

Can we produce either type of forgery in RSA signatures?
Assume Freddy the Forger has access to the public parameters and a valid signed
message.

- Existential:

  We know $(n, b)$.

  Pick a random $y$:
  $$\begin{aligned}
    (y^b \bmod n, y) \to m \equiv y^b \bmod n
  \end{aligned}$$

  So it verifies. $y^b \bmod n$ likely meaningless random string of bits.

  We could also add parity check requirements on $m$ to enhance security.

- Selective:

  Given $m$, find $y$ such that $(m, y)$ verifies.

  Find $y$ such that $y^b \equiv m \pmod n$.
  This is equivalent to finding $y \equiv m^a \pmod n$.
  This is to say, break the corresponding RSA encryption system.

## Blind Signatures

Here is a variation on the signature problem:
- Suppose Bob has made an important discovery.
  He wants to record publicly what he has done (Nobel prize priority), but does
  not want anyone else to know the details at the moment (wants to implement it
  and make a zillion dollars first).
- Alice agrees to sign the message without knowing the content.
  This serves as an affirmation that the contents of the message were in place
  at the time the signature was made.

The message to be signed is $m$.
They do the following:
1. Alice sets up an instance of RSA signatures with public parameters $(n, b)$
   and private parameters $(p, q, a)$.  
   Note: We can now say that the desired signature is $m^a$.
2. Bob chooses random integer $k \in \Zs{n}$ and computes $t = k^bm \bmod n$.
   He sends $t$ to Alice.
3. Alice signs $t$ by computing $s = t^a \bmod n$.
   She returns $s$ to Bob.
   $$\begin{aligned}
     s &\equiv t^a \\
     &\equiv \paren{k^b m}^a \\
     &\equiv k^{ba} m^a \\
     sk^{-1} &\equiv m^a \pmod{n} &\textnormal{correct signed message}
   \end{aligned}$$
   Thus $(m, m^a)$ is valid.
4. Bob computes $sk^{-1} \bmod n$.
   This is the signed message $m^a$.

Bob put a mask on the actual message.
It was designed so that when the message was raised to $b$, the mask was
changed, but in a way that Bob could easily determine the new mask and remove
it.
