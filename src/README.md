# About

This is notes for MATH 4176 (Fall 2021) with Dr. Eric Ufferman.
This should be used in addition to the notes provided by the professor.

# Contributing

The more contributions we have, the better the notes will be.
All contributions, large and small, are appreciated.

The document text and structure is written in [Markdown][markdown].
The math is written in [$\KaTeX$][katex].

Please open a pull request or open issues on [GitLab][gitlab].
If you have never heard of git or markdown, then feel free to message me (@a172)
on [Discord][discord] with things that need fixed or suggestions.

[gitlab]: https://gitlab.com/a172/math4176-notes
[discord]: https://discord.gg/XkspJPAMMs
[katex]: https://katex.org/docs/supported.html
[markdown]: https://www.markdownguide.org/basic-syntax/
