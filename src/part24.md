# Part 24

## Hashing and Signing
- How can we sign a very long message?
- Signing is relatively slow.
- If we are using, e.g. El Gamal, then signing triples the amount of data to be
  sent.
- A solution: Hash each message, and sign the hash.

## Basics of Hash functions

A hash function is a way of compressing data to produce a (usually shorter)
fixed length string known as a _hash_ or _digest_.
Suppose:
$$
  h : \{0, 1\}^\ast \to \{0, 1\}^m
$$
is a hash from binary strings of arbitrary length to strings of a fixed size.
A typical length for _m_ is 256.

A good hash function should have the following properties:
- For any $x$ we can compute $h(x)$ efficiently.
- Preimage resistance.
  Given hash $y$ it should be very difficult to find $x$ such that $h(x) = y$.
- Together, these say $h$ is a one-way function.
  This is absolutely essential.
- Collision resistance.
  Given $h$, it is difficult to find $x_1, x_2$ such that $h(x_1) = h(x_2)$.
  We may think of this a stronger version of preimage resistance.

Notes:
- Hash functions are typically unkeyed.
  Anyone can compute the hash of any input.
- The domain of a hash function is infinite, whereas the range is finite.
  Infinitely many inputs can hash to the same output.

## The Basic Method

There are many well-known hash functions that produce hashes of lengths 160-512
bits.

So we may do the following to sign message $m$ of arbitrary length.
- Choose a hash function $h$, and a signature scheme of our choice.
- The signed message is $(m, y)$.
  Where $y$ is the signature of $h(m)$ in the chosen scheme.
- Verification, compute $h(m)$, and check that $(h(m), y)$ is a valid signed
  message in the scheme.

## Comment on DLP

We'll look at another signature scheme where hashes are incorporated into the
scheme proper.
First, some motivation for the set-up.

- $G$ subgroup of $\Zsp$ where $p$ is a 2048 bit number.
- Suppose we set up an instance of the DLP in a group $G$ of size $q$, where $q$
  is a, say, 224-bit number.
- Both of our generic algorithms for the DLP will be ineffective: each of Shanks
  and Pollard Rho will take about $2^{112}$ operations.
- However, the index calculus may still be effective.
  But if we situate $G$ inside a group $\Zsp$ where $p$ is much larger than $q$,
  then the index calculus will be ineffective.
- This is because the size of the factor base needed depends on $p$ and not $q$.
- This will allow us to use shorter signatures for the same level of security.

## Schnorr Signature Scheme

- Alice chooses primes $p$ and $q$ such that $q \mid p - 1$.
- (Typical sizes for $p$ and $q$ are 2048 and 224 bits respectively)
- Let $\alpha$ be an element of order $q$ in $\Zsp$, and $G = \ang{\alpha}$ so
  that $\abs{G} = q$.
- Set up an instance of DLP:
  - $\beta \= \alpha^a \pmod p$
  - $2 \le a < q$
- publish $(p, q, \alpha, \beta)$, keep $a$ secret.
- Let $h : \{0, 1\}^\ast \to \Z_p$ be a secure hash function.
- To sign message $x$, Alice picks a secret random, $k, 1 \le k < q$.
- Define:
  $$\begin{aligned}
    \sig_{x, k} &= (r, s) \\
    r &= h\paren{x \cat \paren{\alpha^k \bmod p}} \\
    s &= (k + ar) \bmod q
  \end{aligned}$$
- Signed message is $(x, r, s)$
- Verification is done by checking:
  $$r = h\paren{x \cat \paren{\alpha^s \cdot \beta^{-r}} \bmod p}$$

Check:
$$\begin{aligned}
  \alpha^s \cdot \beta^{-r} &\= \alpha^{k + ar} \cdot \alpha^{-ar} &\pmod p \\
  &\= \alpha^k &\pmod p
\end{aligned}$$

So:
$$
  \alpha^s \cdot \beta^{-r} \bmod p = \alpha^k \bmod p
$$

## Security of Schnorr Scheme

What would an attacker need to do to forge a signature?
- Can choose $k$ at will, and compute $r = h \paren{x \cat \alpha^k \bmod p}$.
- Regardless of $k$, the hash function outputs a random-looking integer.
- Because the hash function is pre-image resistant, it is difficult to find any
  other string that hashes to the same value.
- So now must find $s$ such that $\alpha^s \beta^{-r} \= \alpha^k \pmod p$,
  which amounts to knowing $\log_\alpha^p(\beta)$.

This is equivalent to:
$$\begin{aligned}
  \alpha^s \alpha^{-ar} &\= \alpha^k &\pmod p \\
  s - ar &\= k &\pmod q \\
  a &\= r^{-1} (s - k) &\pmod q
\end{aligned}$$

What does a signed message reveal?

Only that $\alpha^s \beta^{-r} \equiv \alpha^k \pmod p$ for some randomly chosen
value of $k$.
With no further information on $k$, it is impossible to glean anything about
$a$.


## The Digital Signature Algorithm
The DSA combines many elements we've already looked at.
It requires hashing and signing.
The signature itself is a variant on ElGamal.
We work in a prime-order subgroup of $\Zsp$ much as in Schnorr's scheme.
We present the details here.

1. Set-up
    1. Alice chooses primes $q, p$ such that $p \= 1 \pmod q$, and $\alpha$ of
       order $q$ mod $p$.
    1. Chooses secret $\alpha \in \{2, q-1\}$ and computes
       $\beta \= \alpha^a \pmod p$.
    1. Publishes $(\alpha, \beta, p, q)$. Keeps $a$ private.
1. Signature
    1. To sign message $x$, Alice first uses a hash function $h$ from SHA
       family, and computes $h(x) = m$, a hash of required length.
    1. Choose $k$ at random in $\Zs{q}$.
    1. Compute $r = \paren{\alpha^k \bmod p} \bmod q$.
    1. Compute $s \=k^{-1} (m + ar) \pmod q$
    1. Signed message is $(x, r, s)$.
1. Verification
    1. Bob computes $m = h(x)$.
    1. Bob computes $u_1 =\ s^{-1}m \pmod q$ and $u_2 \= s^{-1}r \pmod q$.
    1. Compute $v = \paren{\alpha^{u_1} \beta^{u_2} \bmod p} \bmod q$.
    1. Accept iff $v = r$.

### Verifying Verification
We have:
$$\begin{aligned}
  ks &\= m + ar &\pmod q \\
  k &\= s^{-1}m + as^{-1} r &\pmod q \\
  k &\= u_1 + au_2 &\pmod q \\
  \alpha^k &\= \alpha^{u_1} \alpha^{au_2} &\pmod p \\
  \alpha^k &\= \alpha^{u_1} \beta^{u_2} &\pmod p \\
  \paren{\alpha^k \bmod p} \bmod q
    &= \paren{\paren{\alpha^{u_1} \beta^{u_2}} \bmod p} \bmod q \\
  r &= v
\end{aligned}$$
