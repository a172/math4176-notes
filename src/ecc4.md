# Elliptic Curve Cryptography: Part 4

First step: generate an elliptic curve to use.

- Choose large enough prime $P$.
- Choose $a \in \Z_p$.
- Choose a base point $\alpha = (x_0, y_0)$
- Choose $b$ so that ${y_0}^2 \= ({x_0}^3 + a(x_0) + b \pmod p$
- Now $y^2 \= x^3 + ax + b \pmod p$ is a curve with $\alpha$ is a base point.


**Example:** DIffie-Hellman

- A and B agree upon elliptic curve $E$ and base point $\alpha$ as above.
- A chooses a secret $c \in \Z_p$.
- B chooses secret $d \in \Z_p$.
- A computes $G = c\alpha$; B $H = d\alpha$.
- A and B exchange $G, H$.
- $s = cH = dH = cd\alpha$
- Note that $c \alpha$ is $\alpha$ added to itself $c$ times.
  Same with $d$.
  This is just a notation difference stemming from the group being traditionally
  defined with $+$ instead of $\times$.

**Concrete example**

A, B agree upon $p = 7211$
$$\begin{aligned}
  \alpha &= (3, 5) \\
  a &= 1 \\
  y^2 &\= x^3 + x + b &\pmod p \\
  25 &\= 27 + 3 + b &\pmod p \\
  b &\= 7206 &\pmod p
\end{aligned}$$

$E$ is given by:
$$
  y^2 \= x^3 + x + 7206 \pmod p
$$

Alice:
- Picks $c = 13$
- $G = 13\alpha = (1794, 6375)$

Bob:
- Picks $d = 23$
- $H = 23\alpha = (3861, 1242)$
- $s = 23G = 13H = 299\alpha = (1472, 2098)$
