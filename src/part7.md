# Part 7


## In $\Z_7$

|   $b$   |   1   |   2   |   3   |   4   |   5   |   6   |
|---------|-------|-------|-------|-------|-------|-------|
|  $b^2$  | $1^2$ | $2^2$ | $3^2$ | $4^2$ | $5^2$ | $6^2$ |
| reduced |   1   |   4   |   2   |   2   |   4   |   1   |

Full list of QRs: 1, 2, 4

The QNRs are: 3, 5, 6


## In $\Z_{11}$

Only test half, because additive inverses square to the same thing.

| $b$ | 1 | 2 | 3 | 4 | 5 |
|---|---|---|---|---|---|
| $b^2$ | $1^2$ | $2^2$ | $3^2$ | $4^2$ | $5^2$ |
| reduced | 1 | 4 | 9 | 5 | 3 |


## Euler's Criterion

## Theorem

If $p$ is an odd prime, exactly half of the elements of $\Z_p^\ast$ are
quadratic residues.

Claim: $f$ is a 2 to 1 function, ie every $z \in \mathrm{QR}(p)$ there are
exactly two X's in $Z_p^\ast$ s.t. $f(x) = z$.

Note if $z \in \mathrm{QR}(p)$, there is an $x \in \Z_p^\ast$ s.t. $f(x) = z$.
Also:
$$\begin{aligned}
 f(-x) &= (-x)^2 \mod p \\
  &= x^2 \mod p \\
  &= z
\end{aligned}$$

Now suppose $y^2 \equiv z \mod p$.
Then:
$$\begin{aligned}
  y^2 &\equiv x^2 &\pmod p \\
  y^2 - x^2 &\equiv 0 &\pmod p \\
  p &\mid \left(y^2 - x^2 \right) \\
  p &\mid (y - x)(y + x) \\
  p &\mid \begin{cases}
    (y - x) &\textnormal{OR} \\
    (y + x)
  \end{cases} \\
  y &\equiv \pm x &\pmod p
\end{aligned}$$

So f is 2 to 1.


## QUAD-RES

### forward direction
First suppose $a$ is a QR.
Then $a \equiv x^2 \mod p$.
So $a^{\frac{p-1}2} \equiv (x^2)^{\frac{p-1}2} \equiv x^{p-q} \equiv 1 \mod p$.

### backward direction
Suppose $a^{\frac{p-1}2} \equiv 1 \mod p$.
Write $a \equiv b^k \mod p$, where $b$ is a generator.
So $b^{\frac{k(p-1)}2} \equiv 1 \mod p$.
This implies $\textnormal{ord}_p(b) = (p - 1) \mid \frac{k}2(p-1)$,
so $\frac{k}2$ is an integer and $k$ is even.
So $a \equiv b^k \equiv (b^{\frac{k}2})^2 \mod p$, so a is a QR.


## Legendre and Jacobi symbols

$$
\left( \frac27 \right) = 2^3 \mod 7 = 1
$$

$$
\left( \frac2{11} \right) = 2^5 \mod 11 \equiv -1 \mod 11
$$

---

$$4725 = 3^3 \times 5^2 \times 7$$

$$\begin{aligned}
(\frac{989}3)^3 \times (\frac{989}5)^2 \times (\frac{989}7) \\
(\frac23)^3 \times (\frac45)^2 \times (\frac27) \\
(-1)^3 \times 1^2 \times 1 \\
-1
\end{aligned}$$
