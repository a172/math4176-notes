# Part 13

## Random Square's Algorithm

We outline an algorithm that utilizes teh congruence $x^2 \equiv y^2 \pmod n$.
- We choose a factor base $\mathcal{B}$ consisting of the first $k$ primes for
  some value of $k$.
  For example, if $k = 3$, our factor base $\bathcal{B} = \{2,3,5\}$.
- We randomly<sup>&#x2a;</sup> choose $x$, such that $1 < x < N$.
- ...

**Example:**
Let $n = 551$.
Use the factor base $\mathcal{B} = \{2,3,5\}$.
Computing $\mod a^2 \mod n$ for various randomizations $a$:

- $a = 34$
  $$\begin{aligned}
    34^2 &\equiv 1156   &\pmod{551} \\
    &\equiv 54          &\pmod{551} \\
    &\equiv 2 \cdot 3^3 &\pmod{551}
  \end{aligned}$$

- $a = 35$
  $$\begin{aligned}
    35^2 &\equiv 123   &\pmod{551} \\
    &\equiv 3 \cdot 41 &\pmod{551}
  \end{aligned}$$
  Discard 35.

- $a = 52$
  $$\begin{aligned}
    52^2 &\equiv 500      &\pmod{551} \\
    &\equiv 2^2 \cdot 5^3 &\pmod{551}
  \end{aligned}$$

- $a = 55$
  $$\begin{aligned}
    55^2 &\equiv 270            &\pmod{551} \\
    &\equiv 2 \cdot 3^3 \cdot 5 &\pmod{551}
  \end{aligned}$$


We kept:
$$\begin{aligned}
  34^2 &\equiv 2 \cdot 3^3         &\pmod{551} \\
  52^2 &\equiv 2 \cdot 5^3         &\pmod{551} \\
  55^2 &\equiv 2 \cdot 3^3 \cdot 5 &\pmod{551} \\
\end{aligned}$$

Take congruences and multiply all three together (go get all even powers).
$$\begin{aligned}
  (34 \cdot 52 \cdot 55)^2 &\equiv 2^4 \cdot 3^6 \cdot 5^4                &\pmod{551} \\
  (34 \cdot 52 \cdot 55)^2 &\equiv \left(2^2 \cdot 3^3 \cdot 5^2\right)^2 &\pmod{551} \\
  264^2 &\equiv 496^2 &\pmod{551} \\
\end{aligned}$$

Is this useful?
$$ 264 \ne \pm 496 \pmod{551} $$

So, yes.

$$\begin{aligned}
  \gcd(y - x, n) &= \gcd(496 - 264, 551) \\
  &=\gcd(232, 551) \\
  &= 29 \\
  551 &= 29 \cdot 19
\end{aligned}$$

### Improving the Algorithm
