# Part 18

## Index calclus

### Proof

All logs are base $\alpha$:

Write (l_1 + l2)(mod p-1) = (l1 + l2) + k(p-1) for some k in \Z.
Then \alpha^{l_1 = l_2}\pmod{p-1} = \alpha^{(l1+l2)+k(p-1)}
= \alpha^{l1 + l2) \times (\alpha^{p-1})^k = alpha^l1 * alpha^l2 = beta1 beta2

so log(beta1 * beta2) = (l1 + l2)(mod p-1) Since 0 \le (l1 + l2)(mod p-1) < p-1
