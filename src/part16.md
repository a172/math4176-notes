# Part 16

It will be helpful to state the DLP in a more general setting.
Recall that $\Zs{n}$ is an example of a mathematical structure called a group.

## Group definition
A (finite) group $G$ is a (finite) set equipped with binary operation $\cdot$
with properties for any $g, h \in G$:

1. **Associative**
  $$g \cdot h = h \cdot g$$
1. **Closure**
  $$g \cdot h \in G$$
1. **Identity element**  
  There is an element $e \in G$ such that:
  $$e \cdot g = g \quad \forall g \in G$$
1. **Inversion**  
  For every $g \in G$ there is a unique $g^{-1}$ such that:
  $$g \cdot g^{-1} = e$$

Recall the following definitions:
- The order of an element $\alpha \in G$ ($\ord(\alpha)$) is the least positive $n$ such that
  $\alpha^n = e$ (Such an $n$ always exists if $G$ is finite).
- A subgroup $H \subseteq G$ is just a subset of $G$ that happens to also be a
  group under $\cdot$.
- $\ang{\alpha}$ is the subgroup where the elements are the values you can get by
  exponentiating $\alpha$.

## Discrete Log Problem in Groups

Given group $G$, we want to solve $\beta = \alpha^e$ for $e$ where:
- $0 \le e < n$
- $n = \ord(\alpha) \ne \infty$
- $\beta \in \ang{\alpha}$
- $\ang{\alpha} = \{\alpha^i : 0 \le i < n \}$

### Naive approach 1: brute force
Trivially, we can compute $\alpha, \alpha^2, \alpha^3, \ldots$ until $\beta$ is
encountered.

### Naive approach 2: rainbow table
For all $i$, compute the tuple $(i, \alpha^i)$.
Sort this collection of tuples by the second element (a BST is a good option).
Search this collection to find $\beta = \alpha^i$, and return $i$.

Obviously, this approach is only better if we need to find many discrete logs
for the same $\alpha, G$.

Neither naive approach is practical for sufficiently large values of $n$.

## Shank's Algorithm

This method is also known as the Baby step-Giant step algorithm for solving the
discrete log problem.

Let:
- Set $m = \ceil{\sqrt{\ord(\alpha)}}$
- Form two lists:
  - $L_1 = \{ (j, \alpha^{mj}) : 0 \le j < m \}$
  - $L_2 = \{ (i, \beta \cdot \alpha^{-i}) : 0 \le i < m \}$

Find a collision in second component of each list.
This gives:
$$\begin{aligned}
  \beta \cdot \alpha^{-i} &= \alpha^{mj} \\
  \beta \cdot \paren{\alpha^i}^{-1} &= \alpha^{mj} \\
  \beta &= \alpha^{mj} \cdot \alpha^i \\
  \beta &= \alpha^{mj + i} \\
  \log_\alpha(\beta) &=mj + i
\end{aligned}$$
Note that $e = \log_\alpha(\beta) = mq + r$ where $0 \le q < m$, since
$\ord(\alpha) \le m^2$ and $e < \ord(\alpha)$.

We get collision for $j = q$ and $i = r$.

**Example:**
Find $\log_2(13)$ in $\Z_{17}^\ast$.
Use Shanks.

Note:
$$\begin{aligned}
  \alpha &= 2 \\
  \beta &= 13 \\
  2^4 &\equiv -1 \pmod{17} \\
  2^8 &\equiv 1 \pmod{17} \\
  \\
  2^{-1} &\equiv 9 \pmod{17} \\
  \ord_{17}(2) &= 8
\end{aligned}$$

Applying Shanks:
$$\begin{aligned}
  m &= \ceil{\sqrt8} = 3 \\
  L_1 &= \{ (0, 2^0); (1, 2^3); (2, 2^6) \} \\
  L_1 &= \{ (0, 1); (1, 8); (2, 13) \} \\
  L_2 &= \{
    (0, 13 \cdot 2^{-0}),
    (1, 13 \cdot 2^{-1}),
    (2, 13 \cdot 2^{-2})
    \} \\
  L_2 &= \{
    (0, 13 \cdot 1),
    (1, 13 \cdot 9),
    (2, 13 \cdot 13)
    \} \\
  L_2 &= \{
    (0, 13),
    (1, 15),
    (2, 16)
    \} \\
  \beta &= \alpha^{2 \cdot 3 + 0} = \alpha^6
\end{aligned}$$

Note the collision:
$$\begin{aligned}
  L_1[2] &= (2, 13) \\
  L_2[0] &= (0, 13)
\end{aligned}$$

That is:
$$\begin{aligned}
  \beta \cdot \alpha^{-i} &= \alpha^{mj} \\
  \log_\alpha(\beta) &=mj + i \\
  \log_2(13) &= 3 \cdot 2 + 0 \\
  \log_2(13) &= 6
\end{aligned}$$
