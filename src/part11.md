# Part 11

## Pollard Rho Algorithm

An idea for another factoring algorithm:
- Let $n = pq$, where $p$ and $q$ are unknown primes.
- Iterate a "random" map $f : \Z_n \to \Z_n$, starting with some $x_0$.
- So $x_k = f(x_{k-1})$
- Eventually, $f$ is periodic mod $p$, so we'll have $x_i \equiv x_j \pmod p$
  for some $i \ne j$.
- We hope that we can find such $i, j$ where $x_i \not\equiv x_j \pmod n$.
- In that case, $\gcd(x_i - x_j, n) = p$

**Example:**
Let's iterate a map in $\Z_{47}$ (47 is prime) to see what happens.
Let $f(x) = x^2 + 1$, starting at $x_0 = 1$.
Compute the sequence $x_k$.

$$\begin{aligned}
  x_1 &= f(k_0) = f(1) = 2 \\
  x_2 &= f(x_1) = f(2)
\end{aligned}$$

$$\begin{aligned}
  y_0 &= x_0 \\
  y_1 &= x_2 \\
  y_2 &= x_4
\end{aligned}$$

# 2021-09-27

Suppose $n = pq$ and $m_1$ and $m_2$ are indices s.t. $x_{m_1} \equiv x_{m_2}
\pmod p$ and this is first repetition.

$m_2 > m_1$.

What is max # of iterations needed in Pollard Rho?

...

Given $n = pq$, on what iteration would we expect to first see a repetition of
value of $x_k$ in $\Z_p$?

Birthday "Paradox" - if $\ge23$ people are in a room, there is a >50% chance of
at least 2 people sharing a birthday.

Sampling from a set of $p$ objects, once $\approx 1.17\sqrt{p}$ objects are
sampled, a repetition is more likely than not.

Expect in Pollard Rho algorithm $\approx 1.17 \sqrt{p}$ iterations to find
factor $p$.

Note, if $p$ is smaller of two prime factors, $p < \sqrt{n}$.
Expect $\approx 1.17 n^{\frac14}$ iterations.
Compare with trial division, $\approx \frac12 n^{\frac12}$ iterations.

Suppose we want to factor a 30-digit $n$.
$n \approx 10^{30}$.
Trial division takes $\approx 10^15$ iterations.
Pollard Rho takes $\approx 10^{7.5}$ iterations.
However, for a 2048-bit RSA modulus ($n \approx 10^{616}$), neither is effective.
