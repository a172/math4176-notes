# Part 9

## Miller-Rabin

**2021-09-22**

How many consecutive "NO"s do we need for Miller-Rabin test until we're
confident we have a prime?

If $n$ is composite, then at most $\frac14$ of bases in $\Z_n^\ast$ are strong
liars for $n$.

So at most $\frac1{4^k}$ probability of getting $k$ consecutive "NO"s.

- Choose a 500-bit odd number at random.
- Let $C$ be event that the number is composite.
- Let $P$ be the event that the number is prime.
- Note that $C$ and $P$ are complementary.
- Let $N_k$ be event that M-R outputs $k$ consecutive "NO"s (irrespective of
  primality).
- $\mathrm{Pr}(P) = \frac1{173}$ (estimate from previous lecture FOR A 500-bit
  NUMBER)
- $\mathrm{Pr}(C) = \frac{172}{173}$
- $\mathrm{Pr}(N_k) = Pr(P) \times Pr(N_k | P) + Pr(C) \times Pr(N_k | C)$
- $\mathrm{Pr}(N_k) = \frac1{173} \times 1 + \frac{172}{173} \times \frac1{4^k}$

We want to know $\mathrm{Pr}(C | N_k)$.
We will apply Bayes Theorem:
$$\begin{aligned}
\mathrm{Pr}(C | N_k) &= \frac{
  \mathrm{Pr}(N_k | C) \times \mathrm{Pr}(C)
}{
  \mathrm{Pr}(N_k)
}
\\
&= \frac{
  \frac1{4^k} \times \frac{172}{173}
}{
  \frac1{173} + \frac{172}{173} \times \frac1{4^k}
}
\end{aligned}$$

| $k$ |   $\mathrm{Pr}(N_k)$   |
|-----|------------------------|
|  1  | 0.977                  |
|  2  | 0.915                  |
| 10  | 0.000164               |
| 50  | $1.36 \times 10^{-28}$ |

$k = 50$ is the industry standard.
