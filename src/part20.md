# Part 20

## Pohlig-Hellman

We saw that if $2^k$ divides $p - 1$, we can determine the $k$ least significant
bits of a discrete log: $x = \log \beta$.
That is, we can determine $x \bmod 2^k$.
- Apply similar techniques to find the value of a discrete log mod any prime $q$
  that divides $p - 1$.
- Suppose that $3 \mid p - 1$ and $\log(\beta) = x$.
- Write $x$ in _ternary_ as $x_0 + 3x_1 + 9x_2 + \ldots$ where each $x_i$ is a
  ternary digit.
- By our assumption, $\frac{p-1}3$ is an integer, so we can do:
  $$\begin{aligned}
    \beta^\frac{p-1}3
    &= \paren{\alpha^{x_0 + 3x_1 + 9x_2 + \ldots}}^\frac{p-1}3 \\
    &= \paren{\alpha^{x_0 + 3k}}^\frac{p-1}3 \\
    &= \paren{\alpha^{x_0}}^\frac{p-1}3 \paren{\alpha^{3k}}^\frac{p-1}3 \\
    &= \paren{\alpha^\frac{p-1}3}^{x_0} \paren{\alpha^{p-1}}^k \\
    &= \paren{\alpha^\frac{p-1}3}^{x_0}
  \end{aligned}$$
  Let
  $$\begin{aligned}
    \gamma &= \alpha^\frac{p-1}3
  \end{aligned}$$

We can compute each of $\beta^\frac{p-1}3$ and $\gamma$, and test which of
$\gamma^0 = 1, \gamma, \gamma^2$ gives the left hand side.
If in addition, $3^2 \mid p-1$, let
$$\begin{aligned}
  \beta_1 &= \beta \cdot \alpha^{-x_0} \\
  \beta_1 &= \alpha^{3x_1 + 9m} \\
  \beta_1^\frac{p-1}9 &= \paren{\alpha^\frac{p-1}3}^{x_1} \cdot \paren{\alpha^{p-1}}^m \\
  \beta_1^\frac{p-1}9 &= \gamma^{x_1}
\end{aligned}$$

- If $3^k$ is the maximum power of 3 that divides $p-1$, we can use this
  technique to find $k$ ternary digits of $x$.
- I.e., we can completely determine the value $x \bmod 3^k$.
- We can repeat this for other prime divisors of $p - 1$.
  If we can factor $p-1$ into small primes that we can easily find discrete logs
  in $\Zs{p}$, as we will see in the following example.

### Example

Let $p = 181$ and $\alpha = 2$ (which is a primitive mod $p$).
Find $x = \log(131)$.
Note that $\phi(p) = 180 = 2^2 \cdot 3^2 \cdot 5$.
- Write the binary expansion $x = x_0 + 2x_1 + 4x_2 + \ldots$

  Find 1st two bits of $x$: $x_0, x_1$

  $\beta^{\frac{p-1}2} \equiv 131^90 \equiv - 1 \pmod p$

  So, $x_0 = 1$

  <!-- TODO insert 2021-10-22T14:13-0400 image -->

- Write the ternary expansions $x = y_0 + 3y_1 + \ldots$

  Recall $\gamma = \alpha^{\frac{p-1}3} = 2^{60} \equiv 48 \pmod{181}$.

  $$\begin{aligned}
    \gamma^0 &= 1 \\
    \gamma^1 &= 48 \\
    \gamma^2 &= 132 \\
  \end{aligned}$$

  Now:
  $$ \beta^{p-1}3 \equiv 131^{60} \equiv 48 \pmod{181} $$
  So $y_0 = 1$.
  $$\begin{aligned}
    \theta_1  = \beta \cdot \alpha^{-1} \equiv 131 \cdot 2^{-1} \equiv 156 \\
    \theta_1^{\frac{p-1}9 \equiv 156^{20} \equiv 132 \pmod{181} \\
  \end{aligned}$$

  So, $y_1 = 2$.

  Conclude $x \equiv 7 \pmod 9$

  $y_0 + 3y_1 = 1 + 3 \cdot 2 = 7$

- Finally, Let's find $x \bmod 5$.

$$ x = z_0 + 5z_1 + \ldots $$

note that $\beta^{p-1}5 \equiv \paren{\alpha^{z_0 + 5k}^{\frac{p-1}5} \equiv \paren{\alpha^\frac{p-1}5}^z_0 \pmod{181}$

$\epsilon = \alpha^{\frac{p-1}5 \equiv 2^{36} \equiv 157$
$\epsilon^2 = 42$
...

Now solve the system.

$$\begin{aligned}
  x &\equiv 3 \pmod 4 \\
  x &\equiv 7 \pmod 9 \\
  x &\equiv 3 \pmod 5 \\
\end{aligned}$$

It has unique solution $x \equiv 43 \pmod{180}$.

## Setting up a secure instance of DLP

Two questions:
- How can we set up an instance of DLP that is Secure?
  (In particular, secure against Pohlig-Hellman)?
- Related: How can we find primitive (or other high order) elements
  $\alpha \in \Zs{p}$ of higher order?
  We've been assuming that $\alpha$ is primitive, but also have noted there is
  not a good algorithm for finding primitive $\alpha$.

Use Sophie Germain primes.
A prime _q_ is Sophie Germain if $2q + 1$ is also prime.
It is believed but not proved that there are infinitely many such pairs of
primes.

**Example:** $q = 11$ is a S.G. prime, since $p = 2q + 1 = 23$ is also prime.

So in $\Zs{p} = \Zs{23}$:
$$ \abs{\Zs{23}} = 22 = 2 \cdot 11 $$

Possible orders of elements in $\Zs{23}$ are 1, 2, 11, and 22.

## Order of elements

Suppose that $p = 2q + 1$, $q$ a prime.
Possible orders: $1, 2, q, 2q$.
- What elements have order 1?

  Only $x = 1$

- What elements have order 2?

  Only $x = p-1$

- How can we distinguish elements of order $q$ and $2q$?
  $$
    x \ne \pm 1 \\
    \ord(x) = q \iff x^2 \equiv 1 \pmod p
  $$

  - How many of each are there?

    Half of remaining have order $q$.
    Other half have order $2q$.

  - How might we go about finding a primitive element?

    Use a random algorithm.

- Is it better to use a primitive or element of order $q$ as a base in a DLP?

  Use large order $q$, not a primitive element.

### Example

Note $q = 131$ is an S.G. prime.
Find a primitive element and an element of order $q$ in $\Zs{p}$ where
$p = 2q + 1 = 263$.

Check $\alpha = 2$:
$$\begin{aligned}
  2^{131} &\equiv 1 \pmod{263} \\
  \ord(2) &= 131
\end{aligned}$$

Check $\alpha = 3$:
$$\begin{aligned}
  3^{131} &\equiv 1 \pmod{263} \\
  \ord(3) &= 131
\end{aligned}$$

No need to check 4, because a QR cannot be a primitive element.

Check $\alpha = 5$:
$$\begin{aligned}
  5^{131} &\equiv -1 \pmod{263} \\
  \ord(5) &= 262
\end{aligned}$$

5 is primitive.
