# Part 15

## One-way functions and security

- Recall that a one-way function is a function that is easy to compute but
  difficult to invert.
- The security of RSA is based on the ideas that multiplication is a one-way
  function: factorization is difficult.
- Are there other one-way functions we can use to design cryptosystems?

### Discrete Log Problem

- We give simplified formulation of what's known as the Discrete Log Problem
  (DLP)
- Let $n$ be an integer, $\alpha$ a primitive element of $\Z_n^\ast$
- Modular Exponentiation:

  Computing $\alpha^x \bmod n$ is efficient using square-and-multiply.

- Inverting Exponentiation:

  Given $y \in \Z_n^\ast$, find $x$ such that:
  - $\alpha^x \equiv y \pmod n$
  - $0 \le x < \phi(n)$

  If $\alpha$ is not primitive, let $\ord(\alpha) = k$.
  Require:
  - $\alpha^x \equiv y \pmod n$
  - $0 \le x < k$

**Example:**
Let's denote the discrete log of $x$ in $\Zs{n}$ to base $\alpha$ by
$\log_{\alpha}^n(x)$ iff $x \equiv a^y \pmod n$.

In $\Zs{13}$, we have that $\alpha = 2$ is primitive and its powers in
order are:
$$2, 4, 8, 3, 6, 12, 11, 9, 5, 10, 7, 1$$

- What is $\log_2^{13}(11)$?

  $$\log_2^{13}(11) = 7 \because 2^7 \equiv 11 \pmod{13}$$

- What is $\log_2^{13}(1)$?
  $$\begin{aligned}
    \log_2^{13}(1) &= 0 \\
    2^0 &\equiv 1 \pmod{13} \\
    0 &\le 0 < 12
  \end{aligned}$$

- What is $\log_3^{13}(11)$?

  First note that the powers are:
  $$3, 9, 1, 3, 9, 1, 3\ldots$$
  Since 11 does not appear in this list, $\log_3^{13}(11)$ does not exist.

## Diffie-Hellman Key Exchange

- RSA is a full-fledged cryptosystem - we can use it to send any message that
  can be encoded as an integer in $\Z_p$, or using several integers in blocks.
- Another application would be to agree upon a key to be used in a symmetric
  (private-key) cryptosystem
- Before we describe a cryptosystem based on the DLP, we show how it can be used
  to remotely agree upon a key.
- The idea is that they want to establish a secret shared between the two that
  will be difficult for anyone else to ascertain.
  - Alice and Bob agree upon a prime modulus $p$ and a primitive element $a$ in
    $\Zs{p}$.
    (This can be pre-arranged or done publicly)
  - Alice chooses an exponent $x$ at random $1 < x < \phi(p) = p - 1$.
  - Bob chooses an exponent $y$ in same manner.
  - Alice sends $g = \alpha^x \bmod p$ to Bob.
  - Bob sends $h = \alpha^y \bmod p$ to Alice.
  - The shared secret is $s = \alpha^{xy} \bmod p$.
    - Alice computes it as $h^x$.
    - Bob computes it as $g^y$.
  - Only things that are secret are $x$ and $y$, one of which is required to
    derive the secret $s$.
  - The security comes from the belief that the discrete log problem is hard.

**Example:**
Let $p= 101$, and $\alpha = 2$.
Suppose Alice chooses exponent $x = 17$ and Bob chooses $y = 23$.
Find the shared secret in Diffie-Hellman.

Alice:
$$\begin{aligned}
  g &= 2^{17} \bmod 101 = 75 \\
  s &= 53^{17} \bmod 101 = 29
\end{aligned}
$$

Bob:
$$\begin{aligned}
  h &= 2^{23} \bmod 101 = 53 \\
  s &= 75^{23} \bmod 11 = 29
\end{aligned}$$

### Limitation

This is susceptible to a Man in the Middle (MitM) attack, where an attacker
(Mike) pretends to the other member of the party to each Alice an Bob.
Mike then establishes a shared secret with each Alice and Bob and is able to
decrypt and/or modify the messages between them before proxying them on.

## The Elgamal Cryptosystem

The Elgamal cryptosystem is based on the Diffie-Hellman Key Exchange.
The difference is that a new shared secret is established in every
communication.

- Key Generation:
  - Bob chooses a prime $p$, a primitive $\alpha$ in $\Zs{p}$, and a random
    $1 < y < p - 1$ and computes $h = \alpha^y$.
  - Bob publishes the public key $(p, \alpha, h)$ and keeps $y$ private.
- Encryption:
  - Idea:
    - Alice will generate a shared secret in the same manner as in
      Diffie-Hellman Key Exchange.
    - She'll use the shared secret to mask the message, and send the necessary
      info to Bob so that she can also compute the shared secret and unmask the
      message.
    - All calculations are in $\Zs{p}$.
  - Message is converted into a integer $m \in \Zs{p}$.
  - Alice chooses random $x$, where $1 < x < p - 1$.
  - Alice computes $g = \alpha^x$.
  - Alice calculates shared secret $s = h^x$.
  - The ciphertext is the pair $(c_1, c_2) = (g, ms)$.
    Alice sends this to Bob.
- Decryption
  - Bob computes shared secret $s$ as $c_1^y$.
  - Bob recovers message as $m = c_2 \cdot s^{-1}$

**Example:**
Suppose Bob publishes $(p, \alpha, h) = (101, 2, 53)$.

Alice want to send plaintext message $m = 34$ using $x = 17$.
What are the steps of the exchange?

Alice:
$$\begin{aligned}
  s &= h^x \equiv 53^{17} \equiv 29 &\pmod{101} \\
  c_2 &\equiv ms \equiv 34 \cdot 29 \equiv 77 &\pmod{101} \\
  c_1 &\equiv 2^{17} \equiv 75 &\pmod{101}
\end{aligned}$$

Alice sends $(75, 77)$ to Bob.

Bob:
$$\begin{aligned}
  s &\equiv 75^{23} \equiv 29 &\pmod{101} \\
  s^{-1} &\equiv 29^{-1} \equiv 7 &\pmod{101} \\
  m &\equiv c_2 \cdot s^{-1} \equiv 77 \cdot 7 \equiv 34 &\pmod{101}
\end{aligned}$$

### Attacking the Elgamal system

Try to find $y$ from $h, \alpha, p$.
That is, $\log_\alpha^p(h)$.
