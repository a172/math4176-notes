# Part 26: Remote Coin Flipping

## Calculating Square Modulo $n$ Redux
- We saw in the previous notes that if $n = pq$ and we don't know how to factor
  $n$, then we cant compute square roots in $\Zsn$.
- What if we know the factorization of $n$?
  Can we find square roots then?
- The first step is to be able to compute square roots in $\Zsp$ and $\Zs{q}$.
- Recall: If $n = pq$, $p$ and $q$ are distinct odd primes, then $y \in \Zsn$ is
  a quadratic residue mod $n$ if and only if $y$ is a QR mod each $p$ and $q$.
  Moreover, $y$ has four square roots modulo $n$.
  If $y \= x^2 \pmod p$ and $y \= z^2 \pmod q$, then we can recombine these into
  four different systems and four distinct solutions mod $n$ using CRT.
- How do we actually calculate square roots mod a particular prime?
  If $p \= 3 \pmod 4$, it is easy to do.

**Proposition:**
If $p$ is a prime such that $p \= 3 \pmod 4$ and $y$ is a QR mod $p$, then $-y$
is not.
(Note since exactly half of the elements of $\Zsp$ are QRs, this actually
implies that exactly one of $x$ and $-x$ is a QR for any $x \in \Zsp$.)

**Proof:**

Note we can write:
$$
  p = 4k + 3
$$
Also:
$$
  \paren{-1}^\frac{p-1}2 \= (-1)^{2k + 1} \= -1 \pmod p
$$

Now if $y$ is a QR iff:
$$
  y^\frac{p-1}2 \= -1 \pmod p \\
  (-y)^\frac{p-1}2 \= (-1)^\frac{p-1}2 \cdot y^\frac{p-1}2 \= -1 \pmod p\\
  -y \textnormal{ is a QNR}
$$

**Proposition:**
Let $p \= 3 \pmod 4$ and $y$ be a quadratic residue, and let
$x \= y^{\frac{p+1}4} \pmod 4$.
Then $y \= x^2 \pmod p$.

**Proof:**

Note:
$$\begin{aligned}
  x^2 &\= \paren{p^\frac{p+1}2}^2 &\pmod p \\
  &\= y^\frac{p+1}2 &\pmod p \\
  &\= y^\frac{p-1}2 \cdot y &\pmod p \\
  &\= y &\pmod p
\end{aligned}$$

## Coin Flips

Now suppose Alice is in Albuquerque and Bob is in Brussels.
Their great aunt Birgit has bequeathed them in a car.
They want to decide who gets the car.
Bob phones Alice and says he'll flip a coin.
Alice chooses tails, but Bob says sorry it was heads.
Bob gets the car.

Alice talks to her local cryptographer, and comes up with a plan of action for
the next inheritance.

Here's the protocol:
- Alice chooses large primes $p$ and $q$ such that $p \ne q$, but
  $p \= q \= 3 \pmod 4$.
- Alice computes $n = pq$ and sends it to Bob.
- Note that $n$ is public, but only Alice knows $p$ and $q$.
- Bob chooses $x \in \Zsn$ at random (this is the flip) and computes
  $y \= x^2 \pmod n$, and sends $y$ to Alice.
- Using knowledge of the factorization of $n$, Alice can compute all four square
  roots of $y \bmod n$.
  Call them $\pm a$ and $\pm b$.
  One of these must be $x$.
- Alice chooses one of them (say $a$) and sends it to Bob.
  (This is the call.
  Alice is really choosing $\pm a$.)
- If $x \= \pm a \pmod n$, then Alice's call is correct.
  She winds the toss.
  Otherwise, Alice loses.

## Potentials Issues
- How can Alice be sure Bob doesn't cheat?
  If neither of $\pm a$ were Bob's "flip", then Bob can produce one of the other
  pair of square roots $\pm b$.
  Could Bob find the other square root anyway?
  Alice has sent him no new information.
  If his flip was $a$, and he also knew $b$, he would be able to find a
  nontrivial factor of $n$ as $\gcd(a - b, n)$.
  So finding $b$ is at least as difficult as factoring $n$.
- What if Alice tries to cheat by sending a random number other than a square
  root of $y$?
  Bob can easily check whether it's a square root of $y$.
- What if Alice sends a product of three (or more primes)?
  She'd only be decreasing her chances of winning.
- What if Bob intentionally loses?
  This is a possibility.
  The game should be set up so that the only benefit is from winning as
  described by the protocol.

**Example:**
- Let $n = 77$, which is a product of two primes of the requisite form.
- Bob chooses randomly $x = 58$.
- Then $58^2 =\ 53 \pmod{77}$.
  Bob sends 53 to us.
- We can easily determine all four square roots by noting any square root $z$
  satisfies $z \= \pm 2 \pmod 7$ and $z \= \pm 3 \pmod{11}$.
- We solve $z \= 2 \pmod 7$ and $z \= 8 \pmod{11}$ and find the solution
  $z = 30$ (and its negation $z = 47$).
- We solve $z \= 2 \pmod 7$ and $z \= 3 \pmod{11}$ we get $z = 58$
  (and $z = 19$).
- If we call $(19, 58)$ we win, if we call $(30, 47)$ we lose.

$$\begin{aligned}
  53 &\= 4 \pmod 7 \\
  53 &\= 9 \pmod 11 \\
  z^2 &\= 53 \pmod 77 \\
  z^2 &\= 4 \pmod 7 \\
  z^2 &\= 9 \pmod{11} \\
  z^2 &\= \pm2 \pmod 7 \\
  z^2 &\= \pm3 \pmod{11} \\
\end{aligned}$$

Choose say:
$$\begin{aligned}
  z &\= 2 \pmod 7 \\
  z &\= -3 \pmod{11} \\
  z &\= 30 \pmod{77}
\end{aligned}$$

We can reuse $n$ as long as we win the flip.
Losing the flip means revealing two distinct squares, which allows Bob to factor
$n$.
