# Part 10

## Pollard $p - 1$

- Input: odd integer $n > 3$ and a bound $B \ge 2$
- Choose random $a_0 \in \Z_n^\ast$, usually $a_0 = 2$.
- Let $a = a_0$.
- For $j$ in $\mathrm{range}(2, B + 1)$:
  - $a = a^j \mod n$
  - $d = \gcd(a - 1, n)$
  - If $(1 < d < n)$ then return $d$
- Else return FALSE

Suppose $n = pq$, $p, q$ odd primes.
Suppose we want to find p.
If $a \in \Z_p^\ast$, then $a^{\phi(p)} \equiv a^{p-1} \equiv 1 \mod p$.
Also if $\phi(p) \mid k$, then $a^k \equiv 1 \mod p$.
This implies $p \mid a^k - 1$.

Note that after the $j$ iteration, $a \equiv (a_0)^{j!} \mod n$.
We hope at some point $\phi(p) \mid j!$.
Then $p \mid (a - 1)$.

### Example: Factor n = 540143 using Pollard's $p - 1$ method.

We expect to find $p = 421$ when $\phi(p) \mid j!$.
Note $\phi(p) = 420 = 2^2 \times 3 \times 5 \times 7$.

Minimum $j$ such that $\phi(p) \mid j!$.

$q = 1283, \phi(q) = 1282 = 2 \times 641$.
$\phi(q) \mid j!$ only if $j \ge 641$.

This hints at how to pick a secure $p$ and $q$.
We want _both_ of them to be sufficiently large to not be weak to this approach.
