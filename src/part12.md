# Quadratic-based Factoring

A whole slew of factoring algorithms are based on the following simple
observation:

$$
  \begin{aligned}
    x^2 &\= y^2 &\pmod n \\
    x &\not\= \pm y &\pmod n
  \end{aligned}
  \implies
  \begin{aligned}
    n &\mid (x - y)(x + y) \\
    n &\nmid x \pm y
  \end{aligned}
$$

Therefore $n$ has non-trivial factor in common with each $x - y$ and $x + y$, so
we find a factor of $n$ by calculating $\gcd(x \pm y, n)$.

## Square roots mod Composites

Recall that any QR has exactly two square roots mod an odd prime.

**Example:**
Find all distinct solutions of
$$ x^2 \equiv 1 \pmod{35} $$

Argue that these are all the solutions in $\Z_{35}$.

$$\begin{aligned}
  x &\equiv 1 \\
  x &\equiv 6 \\
  x &\equiv 29 \equiv -6 \\
  x &\equiv 34 \equiv -1
\end{aligned}$$

Note:
$$
  x^2 \equiv 1 \pmod{35}
  \implies
  \begin{aligned}
    x &\= \pm 1 \pmod 5 \\
    x &\= \pm 1 \pmod 7
  \end{aligned}
$$
since 5 and 7 are prime.

We can take four combinations, e.g., $x \equiv 1 \pmod 5$, and
$x \equiv -1 \pmod 7$.
Each leads to one of four solutions.

Show how different pairs of square roots can be used to factor 35.

Take:
$$\begin{aligned}
  x &= 29 \\
  y &= 1
\end{aligned}$$
then $x^2 \equiv y^2 \equiv 1 \pmod{35}, x \not\equiv \pm x \pmod{35}$

$$\begin{aligned}
  \gcd(x - y, 35) &= \gcd(28, 35) = 7 \\
  \gcd(x + y, 35) &= \gcd(30, 35) = 5
\end{aligned}$$

4 combinations:
- $++$
- $+-$
- $-+$
- $--$

Each gives unique solution mod 35.

Show how different pairs of square roots can be used to factor 35.

$$\begin{aligned}
  x &= 29 \\
  y &= 1 \\
  \gcd(x - y, n) &= \gcd(29 - 1, 35) \\
  &= \gcd(28, 25) \\
  &= 7
\end{aligned}$$

**Example**:
Find all the square roots of 1 in $\Z_{105}$.
Show how to use a pair to factor 105.

$$
  x^2 \equiv 1 \pmod{105}
  \implies
  \begin{aligned}
    x^2 \equiv 1 \pmod 3 \\
    x^2 \equiv 1 \pmod 5 \\
    x^2 \equiv 1 \pmod 7
  \end{aligned}
  \implies
  \begin{aligned}
    x \equiv \pm 1 \pmod 3 \\
    x \equiv \pm 1 \pmod 5 \\
    x \equiv \pm 1 \pmod 7
  \end{aligned}
$$

implies 8 total solutions, 8 combinations.

For $+++$ solution is $x \equiv 1 \pmod{105}$.  
For $---$ solution is $x \equiv 104 \pmod{105}$.  
For $++-$ solution is $x \equiv 76 \pmod{105}$.  
For $--+$ solution is $x \equiv 29 \pmod{105}$.  
For $+-+$ solution is $x \equiv 64 \pmod{105}$.  
For $-+-$ solution is $x \equiv 41 \pmod{105}$.  
For $+--$ solution is $x \equiv 34 \pmod{105}$.  
For $-++$ solution is $x \equiv 71 \pmod{105}$.  

Choose $x = 64$ and $y = 34$:
$$\gcd(64 - 34, 105) = \gcd(30, 105) = 15$$

## Square-roots Mod $n$

It's helpful to know something more about square roots taken with respect to a
modulus $n$.

**Theorem:**

Consider:
$$
  y^2 \equiv a \pmod {p^e} \\
$$

Where:
- $y \in \Z$
- $a \in \Zs{p^e}$
- $p$ is an odd prime
- $e \in \Z^+$

If $\js ap = 1$, then there are exactly two solutions.  
If $\js ap = -1$, then there are no solutions.

**Proof:**

Claim: if $x^2 \equiv y^2 \pmod {p^e}$ then $y \equiv \pm x \pmod{p^e}$.

Note $p^e \mid (y^2 - x^2) \implies p^e \mid (y - x)(y + x)$.
Suppose that:
$$\begin{aligned}
  p &\mid (y - x) \land p \mid (y + x) \\
  p &\mid 2y \\
  p &\mid y
\end{aligned}$$
This contradicts fact that $a \equiv y^2 \in \Zs{p^e}$.
So, we must have either $p^e \mid (y - x)$ or $p^e \mid (y + x) \implies y
\equiv x \pmod {p^e}$ or $y \equiv -x \pmod{p^e}$ then $a$ is a QR mod $p$.
Since half of elements in $\Zs{p^e}$ satisfy
$\displaystyle \left( \frac ap \right) = 1$, these are exactly the QRs.

**Theorem:**
Suppose that $n > 1$ is an odd integer having factorization:
$$
  n = \prod_{i=1}^l p_i^{e_i}
$$
where the $p_i$ are distinct primes and $e_i \ge 1$.
Let $y \in \Z$ and  $\gcd(a, n) = 1$.
Then the congruence $y^2 \= a \pmod n$ has exactly $2^l$ solutions if
$\js{a}{p_i} = 1$ for $1 \le i \le l$, and has not solutions otherwise.

**Proof:**

If $\left( \frac{a}{p_i} \right) = -1$ for any prime factor, $y^2 = a
\pmod{p_i}$ has no solution so neither does $y^2 \equiv a \pmod n$.
If $\left( \frac{a}{p_i} \right) = 1 $ for all $1 \le i \le l$,
$y^2 \equiv a \pmod {p_i^{e_i}}$ has exactly 2 solutions for each $i$, combine
into $2^l$distinct solutions mod $a$ by CRT.

_example:_

How many solutions does $y^2 \equiv 1 \pmod {86625}$ have?

$$\begin{aligned}
  86625 &= 3^2 \times 5^3 \times 11 \\
  l &= 4 \\
  2^4 &\textnormal{ solutions} \\
  16 &\textnormal{ solutions}
\end{aligned}$$

## Use in factoring
If $n$ is odd and has $k$ distinct prime factors, there are $2^{k-1}$ distinct
pairs of square roots of any QR $a \bmod n$.

Suppose we happen to stumble upon two (not necessarily distinct) square roots of
a QR in $\Zs{n}$ at random.
What is the probability we will be able to use these to factor $n$?

Suppose $x$ is the first square root of $a$.
It comes from one pair.
We're successful if $y$ comes from any of the $2^{k-1} - 1$ other pairs.
Probability of success is:
$$
\frac{2^{k-1} - 1}{2^{k-1}} = 1 - \frac{1}{2^{k-1}}
$$

For $k=2$ prob is $1 - \frac12 = \frac12$.
Probability increases as $k$ increases.
