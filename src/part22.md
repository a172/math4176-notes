# Part 22

## Discrete Log Based Schemes

Idea: Alice establishes a discrete log problem in $\Zs{p}$ for some large prime
$p$, i.e., chooses $\alpha, a$ such that $\gcd(a, p-1) = 1$ and
$\beta \equiv \alpha^a \pmod p$.
Suppose Alice wants to sign a message $m \in \Zsp$. The idea is to send an
exponent $x$ such that:
$$\begin{aligned}
  \alpha^m &\equiv \beta^x     &&\pmod{p}     \\
  \alpha^m &\equiv \alpha^{ax} &&\pmod{p}     \\
  m &\equiv ax                 &&\pmod{p - 1} \\
  x &\equiv a^{-1}m            &&\pmod{p - 1}
\end{aligned}$$
Alice sends $(x, m)$ to Bob.
Bob checks $\beta^x \equiv \alpha^m \pmod p$.
Problem:
Given $\beta^x \equiv \alpha^m$, we can infer:
$$\begin{aligned}
  \alpha^{ax} &\equiv \alpha^m &&\pmod p \\
  ax &\equiv m &&\pmod{p-1}
\end{aligned}$$

Can solve for $x$ if $x \in \Zs{p-1}$.
More generally there are $\gcd(x, p-1)$ solutions.
Potentially find and check all solutions.

## The Fix: Elgamal signature scheme

We fix the above by putting an additional layer of security
- As before, Alice publishes $\beta = \alpha^a$, where $a$ is known only to
  Alice.
- Secretly chooses random $k \in \Zs{p-1}$ and let $r \equiv \alpha^k \pmod p$.
  The parameter $r$ will be public, but we'll keep $k$ private.
  This allows us to use $k$, $r$, and $a$ in the calculation of another
  parameter without revealing $a$.
- We want to put a condition that "something" congruent to $a^m \bmod p$.
  The something will have the form:
  $$ \beta^r r^s $$
- So what is $s$?
  We want:
  $$
  \beta^r r^s
  \equiv \alpha^{ar} \alpha^{ks}
  \equiv \alpha^{ar+ks}
  \equiv \alpha^m
  \pmod p
  $$
  So:
  $$ ar + ks \equiv m \pmod{p - 1} $$
  and:
  $$ s \equiv k^{-1} (m - ar) \pmod{p - 1} $$

Note that knowledge of $r$ and $s$ alone isn't enough to find $a$.
One would have to know $k$.

Given verification conditions:
$$\begin{aligned}
  \alpha^m &\equiv \beta^r r^s \pmod p \\
  m &\equiv ar + ks \pmod {p-1}
\end{aligned}$$

Can't solve this for $a$ without knowing $k$.

## Formal Specification of ElGamal Signatures

- Key set-up: Alice chooses a large prime $p$ and primitive root $\alpha$, and
  secret $a$, and computes $\beta \equiv \alpha^a \pmod p$.
  Alice publishes $(p, \alpha, \beta)$.
- Signing message $m$;
  1. Selects random $k$ such that $\gcd(k, p - 1) = 1$.
  1. Computes $r \equiv \alpha^k \pmod p$.
  1. Computes $s \equiv k^{-1} (m - ar) \pmod{p-1}$.
  1. Signed message it the triple $(m, r, s)$.
- Verification:
  1. Check that $\beta^r r^s \equiv \alpha^m \pmod p$

If Emerson can't use the above method to produce a signature of a message _m_
without knowledge of $a$.

> **NOTE** Forger can choose $r$, but it doesn't help.

Attempting to satisfy the condition:
$$
  \beta^r r^s \equiv \alpha^m \pmod p
$$
is equivalent to:
$$
  r^s \equiv \beta^{-r} \alpha^m \pmod p
$$
which means computing a discrete log.

## Reusing $k$

Alice should choose a new random value of $k$ with each document she signs.
Suppose she chooses the same value of $k$ for messages $m_1$ and $m_2$.
The same value $r$ is then used in both signatures, so Freddy sees that it's
been used twice.
The $s$ values $s_1$ and $s_2$ are different.
Freddy knows that:
$$
  s_1k - m_1 \equiv -ar \equiv s_2k - m_2 \pmod {p - 1}
$$
Therefore,
$$
  \paren{s_1 - s_2}k \equiv m_1 - m_2 \pmod{p - 1}
$$
There are $d = \gcd(s_1 - s_2, p - 1)$ solutions for $k$.
They may be found readily, and tested by computing $\alpha^k \mod p$ until $r$
is found.
Freddy now solves:
$$
ar \equiv m_1 - ks_1 \pmod{p-1}
$$
similarly, and which point she's found $a$ and can reproduce Alice's signatures
at will.

### Example
Alice sets up an ElGamal system.
- $p = 227$
- $\alpha = 2$
- $\beta = 224$
- Alice signs messages $m_1 = 66$ and $m_2 = 26$.
  The Signed messages are:
  $$
    (66, 164, 112) \\
    (26, 164, 110)
  $$
- We have:
  $$\begin{aligned}
    112k &\equiv  110k - 226\pmod{p - 1} \\
    2k &\equiv 40 \pmod{226} &\gcd(2, 40) = 2 \implies 2 \textnormal{ solutions}\\
    k &\equiv 20 \pmod{113} \\
  \end{aligned}$$

  Two solutions: $k = 20, 133 \pmod{226}$.
  Check, or note that $k$ must be invertible so $k = 133$.
  $$ \alpha^{133} = 164 \pmod p $$
- However, the forger still needs to find $a$ in order to forge Alice's
  signature.
  Using either of the messages, we may determine:
  $$\begin{aligned}
    m_1  &\equiv ar + ks_1   &&\pmod{p - 1} \\
    ar   &\equiv m_1 - ks_1  &&\pmod{p - 1} \\
    164a &\equiv 86          &&\pmod{226} \\
    82a  &\equiv 46          &&\pmod{113} \\
    a    &\equiv 43 \cdot 51 &&\pmod{113} \\
    a    &\equiv 46          &&\pmod{113} \\
    a    &\equiv 46, 159     &&\pmod{226}
  \end{aligned}$$
  Check:
  $$ \alpha^{159} \equiv \beta \pmod p $$
  So, $a = 159$.

## Generating Random Signed Messages

We saw that in RSA signatures existential - but not selective - forgeries are
possible.
We'll see a similar phenomenon with ElGamal:
- Suppose $i$ and $j$ are integers such that $0 \le i < p - 1$ and
  $0 \le j < p - 1$, and suppose we express $r$ in the form
  $r \equiv \alpha^i\beta^j \pmod p$.
- Then the verification condition is:
  $$ \alpha^m \equiv \beta^r \paren{\alpha^i \beta^j}^s \pmod p $$
  Which is equivalent to:
  $$ \alpha^{m-is} \equiv \beta^{r+js} \pmod p$$
- The easiest way to satisfy the latter congruence is to make both exponents
  congruent to $0 \bmod p - 1$.
  $$\begin{aligned}
    m - is \equiv 0 &&\pmod{p-1} \\
    r + js \equiv 0 &&\pmod{p-1} \\
  \end{aligned}$$
- If $j \in \Zs{p-1}$, we can solve the congruences for $m$ and $s$:
  $$\begin{aligned}
    x &\= -j^{-1} r &&\pmod{p - 1} \\
    m &\= is &&\pmod{p - 1} \\
    m &\= -i j^{-1} r &&\pmod{p - 1}
  \end{aligned}$$

Combining with $r \equiv \alpha^i \beta^j \pmod p$, we get that $(m, r, s)$ is a
valid signature.

**Example:** Alice sets up ElGamal Signature Scheme as:
- $p = 1223$
- $\alpha = 5$
- $\beta = 590$

Forgery is done as follows:
- Randomly choose:
  - $i = 38$
  - $j = 51$
- Then $r \equiv \alpha^{38} \beta^{51} \equiv 269 \pmod{1223}$
- Compute $j^{-1} \equiv 623 \pmod{1223}$
- Then:
  - $s \equiv -269 \cdot 623 \equiv 1049 \pmod{1222}$
  - $m \equiv 1049 \cdot 38 \equiv 758 \pmod{1222}$
- Claim: $(758, 269, 1049)$ is a valid signed message.
- Check: $\alpha^m \equiv \beta^r r^s \equiv 612 \pmod p$, so signature is
  valid.
