# Part 4

## The multiplicative group $\Z_n^{\ast}$

Fact: for any $n > 1, \Zsn$ is a group under multiplication.
Explain why.

1. Associativity: yes
2. Identity: $1^{-1} \in \Z_n^\ast$ so, $1 \in \Z_n^\ast$.
3. If $a \in \Z_n^\ast$ there is $b \in \Z_n^\ast$ s.t. $ab = 1 \in \Z_n^\ast$.
  So, $b = a^{-1} \in \Z_n\ast$.

## Class Exercise 1

1. $\phi(p) = p - 1$
2. Find
  - $\phi(10) = 4$
  - $\phi(15) = 10$
  - $\phi(21) = 12$
3. $(p - 1)(q - 1)$
4. Find
  - $\phi(8) = 4$
  - $\phi(9) = 6$
  - $\phi(25) = 20$
  - $\phi(27) = 18$
5. $\phi(p^k) = p^k - p^k-1$

## Euler phi-function (general)

Yanked from 4175 notes.

Given the prime decomposition of $m$:
$$m = p_1^{r_1}p_2^{r_2}\dots p_k^{r_k}$$

Then:
$$
\phi(m) = m \left(1 - \frac1{p_1}\right) \left(1-\frac1{p_2}\right) \dots \left(1-\frac1{p_k}\right)
$$
