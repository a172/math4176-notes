# Part 5: The RSA Cryptosystem

## Euler-Fermat Theorem

Recall:

- The definition of the Euler-$\phi$ function:
  $$
    \phi(n) = \abs{\{
      a : 1 \le a \le n
      \land
      \gcd(a, n) = 1
    \}}
  $$
  Equivalently:
  $$
    \phi(n) = \abs{\Zsn}
  $$

### Theorem (Euler-$\phi$ value)

Let $n = p_1^{e_1} p_2^{e_2} \dots p_k^{e_k}$ where $k$ are distinct primes.
Then:
$$
  \phi(n) =
    n
    \paren{1 - \frac1{p_1}}
    \paren{1 - \frac1{p_2}}
    \dots
    \paren{1 - \frac1{p_k}}
$$

### Theorem (Euler-Fermat)

$$
  a \in \Zsn \implies a^{\phi(x)} \= 1 \pmod n
$$

**Corollary:**

$$
  z \in \Zsn \implies a^k \= a^{k \bmod \phi(n)} \pmod n
$$

Summary: When working mod $n$, we can reduce $k$ in expressions of form $a^k$
provided:
- $\gcd(a, n) = 1$
- The reduction is done mod $\phi(n)$

Ex:
- Find $2^{1006} \bmod 55$
  $$\begin{aligned}
    55 &= 5 \cdot 11 \\
    \phi(55) &= 55 \paren{1 - \frac15} \paren{1 - \frac1{11}} \\
    \phi(55) &= 55 \cdot \frac45 \cdot \frac{10}{11} \\
    \phi(55) &= 55 \cdot \frac{40}{55} \\
    \phi(55) &= 40 \\
  \end{aligned}$$
  $$\begin{aligned}
    2^{1006} &\= 2^6 &\pmod{55} \\
    2^{1006} &\= 64 &\pmod{55} \\
    2^{1006} &\= 9 &\pmod{55} \\
    2^{1006} \bmod 55 &= 9
  \end{aligned}$$
- Find $21^2 \bmod 125$

  Without even doing any calculations, it is obvious that $2 < \phi(125)$.
  Therefore, we cannot reduce the exponent.
  $$\begin{aligned}
    21^2 &= 441 &\pmod{125} \\
    21^2 &= 66 &\pmod{125} \\
    21^2 \bmod 125 &= 66
  \end{aligned}$$

## Cryptosystems

A _cryptosystem_ consists of:
1. A set of allowable messages, also known as plaintexts $\P$
2. A set of allowable ciphertexts, $\C$.
  (Usually $\P = \C$)
3. A set of possible keys $\K$.
  (Public key: keys that have public and private components)
4. Functions $e_k$ and $d_k$ that tell how to encrypt/decrypt.
  Must have $d_k(e_k(\theta)) = \theta$.

## RSA

We can now describe the RSA cryptosystem.
Plaintexts and Ciphertexts will be in $\Z_n$ (modulus $n$ is defined below).

Bob creates/computes:
- $p, q$: distinct odd primes
- $n = pq$
- $\phi(n) = (p - 1)(q - 1)$
- $a : \gcd(a, \phi(n)) = 1$
- $b \= a^{-1} \pmod{\phi(n)}$

> **NOTE:**
> [Wikipedia][1] suggests using Carmichael's totient function, $\lambda(n)$,
> instead of $\phi(n)$.
> In _this_ case:
> $$\lambda(n) = \frac{\phi(n)}{\gcd(p-1, q-1)}$$

The public key is $(n, a)$.
The private key is $b$.
Additionally, $p$, $q$, $\phi(n)$, and $\lambda(n)$ must be kept private, as if
any one of these are known, $b$ can be derived.

To encrypt message $m$, Alice computes:
$$
  c = e_k(m) = m^a \bmod n
$$

To decrypt $c$, Bob computes:
$$
  d_k(c) = c^b \bmod n
$$

Basic correctness check:
$$\begin{aligned}
  d_k(e_k(m)) &= \paren{m^a}^b \bmod n \\
  &= m^{ab} \bmod n \\
  &= m \bmod n  &&\because b \= a^{-1} \pmod{\phi(n)} \\
  &= m &&\because m \in \Z_n
\end{aligned}$$

[1]: https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Key_generation
