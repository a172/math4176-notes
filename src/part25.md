# Zero Knowledge Proofs

Use Zero Knowledge Proofs (ZKP) when you want to prove
1. That you know something
1. That you can do something
1. Your identity

to another party without:
1. Revealing any information to that party
1. Revealing any information to a third party that may be eavesdropping

**Example:**
- If we don't trust a server to handle our passwords, we may want to use a ZKP
  instead.
- If we don't want to reveal our account number or PIN when withdrawing cash, we
  may want to use a ZKP-based system instead.

## A non-mathematical protocol
- Suppose that Alice is red-green colorblind but Bob is not.
  Alice has a red and green coin that are identical in every way except color.
- Devise a method by which Bob can convince Alice that he can distinguish the
  coins _without revealing to Alice which coin is which_, and without
  introducing any special equipment.
- Is your method foolproof?
  What can be done to make Alice more confident that Bob can actually
  distinguish the coins?

## Mathematical Zero-knowledge Proofs
A ZKP should have the following properties:
1. **Completeness:**
   A true proposition always has a valid proof.
   That is, an honest prover can always convince an honest verifier that the
   claim is true.
   This is the same meaning as in formal logic.
1. **Soundness:**
   A dishonest prover cannot convince an honest verifier that that a false claim
   is true (except with some arbitrarily small probability).
1. **Zero-knowledge:**
   When the protocol is performed, neither the verifier nor any 3rd party gains
   any information they could use to assert the claim.

## A DLP-based version
(Generally, Peggy is the prover, and Victor is the verifier.)

Peggy sets up a secure instance of a DLP: large prime $p$, primitive $\alpha$,
secret $a$, $\beta \= \alpha^a \pmod p$ and publishes $(p, \alpha, \beta)$.
Peggy claims to know $\log_\alpha(\beta)$, and wants to prove this to Victor
without revealing $a$.

The ZKP protocol:

Peggy wishes to prove knowledge of $\log_\alpha(\beta)$ to Victor

1. Peggy chooses random $k$ and sends to Victor $r \= \alpha^k \pmod p$
2. Victor issues one of two challenges.
   In either case, Peggy must find $\log_\alpha(c)$.
   Challenge $c$ can be:
   - $c_1 = r$
   - $c_2 = \beta \cdot r \pmod p$
3. Peggy responds with $\log_\alpha(c)$.
   Note that the responses are (respectively)
   - $k$
   - $a + k \bmod (p - 1)$
4. Victor verifies that $\alpha^x \equiv c \pmod p$, where $x$ is the response.

Note that it is _essential_ that Peggy choose an random $k$ every time, and that
$k$ is never reused.

### Example

Let:
- $p = 101$
- $\alpha = 2$
- $a = 37$
- $\beta \= 2^{37} \= 55 \pmod {101}$

Peggy publishes $(11, 2, 55)$.
Keeps $a$ secret.
1. Peggy chooses $k = 80$.
  $r \= 2^{80} \= 84 \pmod p$.
  Peggy reveals $r = 84$.
2. Victor issues $c_2 \= r \cdot \beta \= 75 \pmod p$
3. Peggy responds with $(a + k) \= 37 + 80 \= 117 \bmod 100 \= 17$
4. Victor checks $2^{17} \= 75 \pmod{101}$

### Checking required properties
1. **Completeness:**
  Peggy can complete all steps as long as she knows $a$.
2. **Soundness:**
  Ivan doesn't know $a$, but wants to complete protocol.
  Ivan chooses $k$, sends $r \= \alpha^k \pmod p$, can respond w/ $k$ given
  challenge $c_1$, but not given challenge $c_2$, since finding $(a + k) \bmod
  (p-1)$ necessitates knowing $a$.  
  What if Ivan anticipates challenge of $c_2$?
  He needs to reply with $x$ such that $\alpha^x \equiv r \cdot \beta \equiv
  \alpha^{k+a} \pmod p$.  
  Instead of sending $r$ in step 1, send $r \cdot \beta^{-1} \pmod p$.
  Then verification is:
  $$
    \alpha^x \equiv r \cdot \beta^{-1} \cdot \beta \equiv r \pmod p
  $$
  Ivan can reply with $k$.
  However, in this case, if the challenge is $c_1$, Ivan would need to provide
  $\log_\alpha\paren{r \cdot \beta^{-1}} = (k - a) \bmod (p - 1)$.
  This requires knowing $a$.
  Therefore, there is a commitment made before the challenge that has a 50%
  chance of being correct.
3. **Zero-Knowledge:** Only random $k$ or $(a + k) \bmod (p - 1)$ is revealed.
  Neither gives any info about $a$.


## Square-root based protocols

Peggy generates large primes $p$ and $q$ and sets $n = pq$.
Peggy chooses a random $s$ in $\Zs{n}$ and computes $y = s^2 \bmod n$.
Peggy publishes $(n, y)$ and claims to know a square root of $y$ in $\Zs{n}$.

Victor wants to verify this, but Peggy does not want to reveal $s$.
Here is the ZKP protocol:
1. Peggy chooses a random number $r_1 \in \Zsn$ and lets
   $r_2 \= s{r_1}^{-1} \pmod n$ so that:
   $$
     r_1r_2 \= s \pmod n
   $$
   Note this implies:
   $$\begin{aligned}
     {r_1}^2 {r_2}^2 &\= s^2 &\pmod n\\
     x_1 x_2 &\= y &\pmod n
   \end{aligned}$$
   She computes:
   $$
     x_1 \= {r_1}^2 \pmod n \\
     x_2 \= {r_2}^2 \pmod n
   $$
   and sends $x_1$ and $x_2$ to Victor.
2. Victor checks that $x_1x_2 \= y \pmod n$ and then issues a challenge
   $c = x_i$ for $i = 1$ or $i = 2$, for which Pegy must supply a square root.
3. Peggy replies with $d = r_i$.
4. Victor checks that $d^2 \= c \pmod n$.
5. The first four steps can be repeated until Victor is convinced.

Checking required properties:
- The arguments for **completeness** and **zero knowledge** are similar to the
  DLP-base scheme:
  - Clearly Peggy can complete the procedure if she knows $s$.
  - The only information that is revealed by a single pass is a square root of a
    randomly generated QR in $\Zsn$.
- What about soundness?
- Can an imposter find $s$?
  Can we compute square roots in $\Zsn$ without knowing the factorization?

  Suppose an imposter can compute square roots of any arbitrary QR in $\Zsn$.
  Reduction of factorization to square root problem:
  - Choose random $x \in \Zsn$.
  - Let $y \= x^2 \pmod n$.
  - Use square root algo to find $z \in \Zsn$ such that $z^2 \= y \pmod n$.
  - 50% probability that $\gcd(z - x, n)$ gives a non-trivial factor of $n$.
  - Therefore, computing a square root is at least as hard as factoring $n$.

- Can an imposter fool the scheme without knowing $s$?

  Ivan could choose random $r_1$, let $x_1 \= {r_1}^2 \pmod n$.
  In order to make sure $x_1x_2 \= y \pmod n$, chooses
  $x_2 \= y \cdot {x_1}^{-1} \pmod n$.
  In case where challenge is $x_2$, Ivan needs to reply with
  $s \cdot {r_1}^{-1} \pmod n$.
  This amounts to knowing a square root of $y$.

## Feige-Fiat-Shamir Identification Scheme
An improvement in terms of efficiency and the number of challenge-and-responses
needed is the Feige-Fiat-Shamir Identification Scheme.

Let $n = pq$ be the product of two large primes.
Peggy has secret numbers $s_1, \ldots s_k$.
Let $v_i \= {s_i}^2 \pmod n$.
The numbers $v_i$ are sent to Victor.
Victor will verify that Peggy knows some subset of the numbers
$s_1, \ldots, s_k$.
The protocol proceeds as follows:
1. Peggy chooses random integer $r$ and computes $x \= r^2 \pmod n$ and sends
   $x$ to Victor.
   (Again, $x$ serves as a mask.)
2. Victor chooses numbers $b_1, \ldots, b_k$ with each $b_i \in \{0, 1\}$ and
   sends these to Peggy.
   (Victor is specifying a subset of the $v_i$ for which he's demanding square
   roots.)
3. Peggy computes $y \= r{s_1}^{b_1} \ldots {s_k}^{b_k} \pmod n$.
4. Victor checks that $y^2 \= x {v_1}^{b_1} \ldots {v_k}^{b_k} \pmod n$.
5. Step 1 through 4 are repeated (each time with a different $r$).

**Example:**

If $k = 10$, challenge could be:
$$b_1 \cat b_2 \cat \ldots \cat b_{10} = \binary{1011011101}$$
indicating Victor wants to confirm
knowledge of $s_1, s_3, s_4, s_6, s_7, s_8, s_{10}$.

Response:
$$ y = r s_1 s_3 s_4 s_6 s_7 s_8 s_{10} $$
Square:
$$ y^2 = x v_1 v_3 v_4 v_6 v_7 v_8 s_{10} \pmod n$$

Note: there are $2^k$ possible challenges Victor could issue.
Ivan can arrange things to be able to reply to exactly one challenge, without
knowing the $s_i$'s. 
This gives Ivan $\frac1{2^k}$ probability of succeeding.
