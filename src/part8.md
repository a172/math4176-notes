# Part 8

## Ex: $n = 113$

$$\begin{aligned}
  n - 1 &= 112 \\
  & = 2^4 \times 7 \\
  (x^{112} - 1) &= (x^{56} - 1)(x^{56} + 1) \\
  \ldots
\end{aligned}$$

## page 2
- $ 1$
- $-1$
- $-1$
- $-1$
- $-1$

## examples

- Let $n = 6601$.
  Then $n - 1 = 2^3 \times 825$.
  Run M-R with $a = 2$.

  $$ b = 2^{825} \mod n = 2738 $$

  Enter for loop:
  $$\begin{aligned}
    i &= 0 & b &= 2738^2 \mod n = 1 \\
    i &= 1 & b &= 1^2 \mod n = 1 \\
  \end{aligned}$$

  Exit loop.
  Return YES (composite), because we never saw $-1$ in the loop.

  $6601 = 7 \times 23 \times 41$ is actually a Carmichael number.

- Let $n = 4033$ for using M-R.

  $$\begin{aligned}
    n - 1 &= 4032 \\
    &= 2^6 \times 63 \\
    a &= 2 & b &= 2^{63} \mod n = 3521 \not\equiv \pm 1 \mod n \\
    i &= 0 & b &= 3521^2 \mod n = 4032 \equiv -1 \mod n \\
  \end{aligned}$$

  Return NO (possibly prime).

  Try again with $a = 3$.


## Another Issue

Let $\theta(n)$ be the # of primes less than $n$.
Then $\phi(n) = \frac{\theta(n)}n$ is the proprotion of primes among the first $n$
positive integers.

### Prime # theorem

$$\phi(n) \approx \frac{n}{\ln(n)}$$.

More precisely:
$$
\lim_{n\to\infty} \frac{\phi(n)}{\frac{n}{\ln(n)}} = 1
$$

So if we randomly choose a 500-bit number, the probability it is prime is about:
$$
\frac{1}{\ln{2^{500}}} \approx \frac1{346}
$$

Only choose odd #s to get \frac1{173} prob. of choosing a prime
